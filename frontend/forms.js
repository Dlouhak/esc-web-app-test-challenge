import $ from 'jquery';
import Nette from 'nette-forms';

Nette.initOnLoad();

Nette.showFormErrors = function showFormErrors(form, errors) {
	const displayed = [];
	let focusElem;

	$(form).removeClass('has-error');
	$(form).find('.is-invalid').removeClass('is-invalid');
	$(form).find('.nette-error').remove();

	for (let i = 0; i < errors.length; i += 1) {
		const elem = errors[i].element;
		const message = errors[i].message;

		$(elem).addClass('is-invalid');
		const group = $(elem).closest('.checkbox, .select, .input-group');
		const targetEl = group.length ? group.get(0) : elem;
		const target = $(targetEl).nextUntil(':not(label)').addBack().last();

		if (!displayed.some(error => error.target === targetEl && error.message === message)) {
			const errorContainer = target.next('.invalid-feedback.nette-error');
			if (errorContainer.length) {
				errorContainer.append('<br />').append(message);
			} else {
				target.after($('<div></div>').addClass('invalid-feedback nette-error').text(message));
			}
			displayed.push({ target: targetEl, message });
		}

		if (!focusElem && elem.focus) {
			focusElem = elem;
		}
	}

	if (focusElem) {
		focusElem.focus();
	}
};


function checkInputValue() {
	$(this).toggleClass('has-value', $(this).val() !== '');
}

$(document).on('change', 'input,textarea', checkInputValue);
$('input,textarea').each(checkInputValue);
