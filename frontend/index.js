import $ from 'jquery';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'bootstrap';

import 'ion-rangeslider';
import 'ion-rangeslider/css/ion.rangeSlider.css';
import 'ion-rangeslider/css/ion.rangeSlider.skinFlat.css';

import './index.scss';
import App from './App';
import store from './store';
import registerServiceWorker from './registerServiceWorker';

const root = document.getElementById('root');

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter basename={`${process.env.PUBLIC_URL}/app`}>
			<App
				userName={root && root.dataset && root.dataset.userName}
				cleanCacheButton={root && root.dataset && !!root.dataset.cleanCacheButton}
			/>
		</BrowserRouter>
	</Provider>,
	root
);

registerServiceWorker();

let windowHeight;

$(function initApp() {
	initFirstPriority();
	initArrowUp();
});

function initFirstPriority() {
	// here handle first priority javascript which effect the visual effect of the page
	windowHeight = $(window).height();
	// when user resize the window - recalculate the window height so anchors / menu etc. will be valid
	window.addEventListener('resize', () => {
		windowHeight = $(window).height();
	});
}

function initArrowUp() {

	// fade in .back-to-top
	$(window).scroll(function () {
		if ($(this).scrollTop() >= windowHeight - 50) {
			$('.back-to-top').addClass("visible").fadeIn();
		} else {
			$('.back-to-top').fadeOut().removeClass("visible");
		}
	});

		// scroll body to 0px on click
	$(document).on('click', '.back-to-top', function (e) {
		e.preventDefault();
		$('back-to-top').addClass('pseudo-hover').delay(1000).queue(function () {
			$('back-to-top').removeClass('pseudo-hover').dequeue();
		});
		$('html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});

}
