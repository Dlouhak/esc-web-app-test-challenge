import * as actionTypes from './tutorialActionTypes';


export function moveReferenceLine(line) {
	return {
		type: actionTypes.MOVE_REFERENCE_LINE,
		payload: line,
	};
}
