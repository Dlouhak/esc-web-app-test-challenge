import { call, put, all, takeEvery } from 'redux-saga/effects';

import {
	fetchEmbryosLibrary,
	fetchMoreEmbryos,

	fetchLearning,
	visitLearning,

	fetchUserProgress,
	fetchEmbryosTraining,
	fetchEmbryosTrainingResult,
	fetchEmbryosTest,
	fetchEmbryosTestResult,
} from '../services/api';

import { actions as embryosLibraryActions, types as embryosLibraryTypes } from '../reducers/embryos_library_reducer';
import { actions as embryosLearningActions, types as embryosLearningTypes } from '../reducers/embryos_learning_reducer';
import { actions as embryosTrainingActions, types as embryosTrainingTypes } from '../reducers/embryos_training_reducer';


/* Embryos Library */

function* onFetchEmbryosLibrary(action) {

	try {

	  const data = yield call(fetchEmbryosLibrary, action.limit, action.embryoId);
	  yield put(embryosLibraryActions.requestSucceededEmbryos(data));

   } catch (error) {

	  yield put(embryosLibraryActions.requestFailedEmbryos(error.message));

   }

}


function* onFetchMoreEmbryos(action) {

	try {

		const data = yield call(fetchMoreEmbryos, action.cluster, action.offset, action.limit);
		yield put(embryosLibraryActions.requestSucceededEmbryosLoadMore(action.category, action.cluster, action.offset, data));

	} catch (error) {

		yield put(embryosLibraryActions.requestFailedEmbryosLoadMore(error.message));

	}

}


/* Learning */

function* onFetchLearning() {

	try {

	  const data = yield call(fetchLearning);
	  yield put(embryosLearningActions.requestSucceededEmbryosLearning(data));

   } catch (error) {

	  yield put(embryosLearningActions.requestFailedEmbryosLearning(error.message));

   }

}

function* onVisitLearning(action) {
	try {
		yield call(visitLearning, action.section);
	} catch (error) {
		// do nothing
	}
}


/* Training */

function* onFetchUserProgress(action) {

	try {

		const data = yield call(fetchUserProgress, action.payload);
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingUserProgress(data));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingUserProgress(error.message));

	}

}


function* onFetchUserProgressAndVisitedLevel(action) {

	try {

		const levelToShow = action.payload;
		const data = yield call(fetchUserProgress, levelToShow);
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingSelectLevel(data, levelToShow));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingSelectLevel(error.message));

	}

}


function* onFetchTrainingQuestion(action) {

	try {

		const { progress, question } = yield all({
			progress: call(fetchUserProgress, action.payload),
			question: call(fetchEmbryosTraining, action.payload),
		});
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingUserProgress(progress));
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingQuestion(question));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingQuestion(error.message));

	}

}


function* onFetchTrainingQuestionResult(action) {

	try {

		const data = yield call(fetchEmbryosTrainingResult, action.id, action.answer);
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingQuestionResult(data));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingQuestionResult(error.message));

	}

}


function* onFetchTrainingTest() {

	try {

		const data = yield call(fetchEmbryosTest);
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingTest(data));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingTest(error.message));

	}

}


function* onFetchTrainingTestResult(action) {

	try {

		const data = yield call(fetchEmbryosTestResult, action.id, action.answers);
		yield put(embryosTrainingActions.requestSucceededEmbryosTrainingTestResult(data));

	} catch (error) {

		yield put(embryosTrainingActions.requestFailedEmbryosTrainingTestResult(error.message));

	}

}



function* mySaga() {

	/* Embryos Library */

	yield takeEvery(embryosLibraryTypes.EMBRYOS_LOAD_MORE_REQUESTED, onFetchMoreEmbryos);

	yield takeEvery(embryosLibraryTypes.EMBRYOS_LIBRARY_REQUESTED, onFetchEmbryosLibrary);


	/* Learning */

	yield takeEvery(embryosLearningTypes.EMBRYOS_LEARNING_REQUESTED, onFetchLearning);
	yield takeEvery(embryosLearningTypes.EMBRYOS_LEARNING_VISITED, onVisitLearning);

	/* Training */

	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_USER_PROGRESS_REQUESTED, onFetchUserProgress);

	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_TRAINING_QUESTION_REQUESTED, onFetchTrainingQuestion);
	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_REQUESTED, onFetchTrainingQuestionResult);

	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_TRAINING_TEST_REQUESTED, onFetchTrainingTest);
	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_REQUESTED, onFetchTrainingTestResult);

	yield takeEvery(embryosTrainingTypes.EMBRYOS_TRAINING_SELECT_LEVEL_REQUESTED, onFetchUserProgressAndVisitedLevel);

}

export default mySaga;
