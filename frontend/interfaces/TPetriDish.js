/* @flow */
export type TPetriDish = {
  id: string,
  label: string
};
