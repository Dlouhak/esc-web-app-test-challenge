/* @flow */
export type TCbHairColor = {
  id: string,
  label: string
}
