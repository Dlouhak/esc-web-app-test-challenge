/* @flow */
export type TCbAction = {
  id: string,
  label: string,
  color: string,
  highlightColor: string
}
