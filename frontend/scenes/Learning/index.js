import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { NavLink, withRouter } from 'react-router-dom';

import Menu from './components/Menu';
import Router from './components/Router';
import ArrowRight from '../../components/ArrowRight';

import Loading from '../../components/Loading';

import { actions } from '../../reducers/embryos_learning_reducer';

import { goToTop } from 'react-scrollable-anchor';

import './styles.scss';

export const LEARNING_PATHNAME = "/learning";
export const INTRODUCTION_PATHNAME = "/learning/introduction";
export const STAGES_OF_DEVELOPMENT_PATHNAME = "/learning/stages_of_development";
export const EMBRYO_QUALITY_PATHNAME = "/learning/embryo_quality";
export const EMBRYO_EVENTS_PATHNAME = "/learning/events";
export const IMPLANTATION_CHANCE_PATHNAME = "/learning/implantation_potential";

class Learning extends Component {

	constructor(props) {

		super(props);

		this.onBackClick = this.onBackClick.bind(this);
		this.onNextClick = this.onNextClick.bind(this);

	}

	componentDidMount() {

		// Get Learning Section Data
		this.props.dispatch(actions.requestEmbryosLearning());

	}


	getSectionTitle() {

		let result = "";
		const pathname = this.props.location.pathname;

		// Introduction
		if(pathname === LEARNING_PATHNAME) {
			result = "CATI grading - evaluation of D5 embryos";
		}

		// Other Sections
		else {
			result = "Evaluation of D5 embryos";
		}

		return result;

	}

	getNextURL() {

		let result = "";
		const pathname = this.props.location.pathname;

		// Introduction
		if(pathname === LEARNING_PATHNAME) {
			result = STAGES_OF_DEVELOPMENT_PATHNAME;
		}

		// Introduction
		if(pathname === INTRODUCTION_PATHNAME) {
			result = STAGES_OF_DEVELOPMENT_PATHNAME;
		}

		// Stages of Development
		else if(pathname === STAGES_OF_DEVELOPMENT_PATHNAME) {
			result = EMBRYO_QUALITY_PATHNAME;
		}

		// Quality
		else if(pathname === EMBRYO_QUALITY_PATHNAME) {
			result = EMBRYO_EVENTS_PATHNAME;
		}

		// Events
		else if(pathname === EMBRYO_EVENTS_PATHNAME) {
			result = IMPLANTATION_CHANCE_PATHNAME;
		}

		return result;

	}

	getBackURL() {

		let result = "";
		const pathname = this.props.location.pathname;

		// Stages of Development
		if(pathname === STAGES_OF_DEVELOPMENT_PATHNAME) {
			result = LEARNING_PATHNAME;
		}

		// Embryo Quality
		else if(pathname === EMBRYO_QUALITY_PATHNAME) {
			result = STAGES_OF_DEVELOPMENT_PATHNAME;
		}

		// Events
		else if(pathname === EMBRYO_EVENTS_PATHNAME) {
			result = EMBRYO_QUALITY_PATHNAME;
		}

		// Implantation Chance
		else if(pathname === IMPLANTATION_CHANCE_PATHNAME) {
			result = EMBRYO_EVENTS_PATHNAME;
		}

		return result;

	}

	getVisitedSections() {

		let result = [];
		const learning = this.props.learning;

		if (learning.implantationChanceVisited) {
			result.push(IMPLANTATION_CHANCE_PATHNAME);
		}

		if (learning.eventsVisited) {
			result.push(EMBRYO_EVENTS_PATHNAME);
		}

		if (learning.embryoQualityVisited) {
			result.push(EMBRYO_QUALITY_PATHNAME);
		}

		if (learning.stagesOfDevelopmentVisited) {
			result.push(STAGES_OF_DEVELOPMENT_PATHNAME);
		}

		if (learning.introductionVisited) {
			result.push(LEARNING_PATHNAME);
		}

		return result;

	}

	onBackClick() {
		goToTop();
	}

	onNextClick() {
		goToTop();
	}

	render() {

		const learning = this.props.learning;

		if (learning !== undefined && Object.keys(learning).length > 0) {

			const title = this.getSectionTitle();

			const backURL = this.getBackURL();
			const backButton = (this.props.location.pathname !== LEARNING_PATHNAME) ?
								<NavLink to={{
											pathname: backURL
										}}
										className="step-back"
										onClick={this.onBackClick}

									><ArrowRight direction="left"/>One step back</NavLink> :
								null;

			const nextURL = this.getNextURL();
			const continueButton = (this.props.location.pathname !== IMPLANTATION_CHANCE_PATHNAME) ?
								<NavLink to={{
											pathname: nextURL
										}}
									className="btn btn-orange continue"
									onClick={this.onNextClick}
									>Continue learning<ArrowRight direction="right"/></NavLink> :
								null;

			const visitedSections = this.getVisitedSections();

			return (
				<div className="container" id="learning-container">

					<h1 className="learning">{title}
					<a href={`${process.env.PUBLIC_URL}/EMBRYOS_CLUSTERS_WHITE_PAPER.pdf`} className="download-button" download="EMBRYOS_CLUSTERS_WHITE_PAPER.pdf">
							<p>Get educational PDF</p>
							<div className="download-thick-icon-wrapper">
								<svg width="14px" height="17px" viewBox="0 0 14 17">
									<g id="Guidelines" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
										<g id="Icons" transform="translate(-671.000000, -1523.000000)">
											<g id="icon-download" transform="translate(666.000000, 1520.000000)">
												<polygon id="Stroke-1" strokeOpacity="0.00784313771" stroke="#000000" strokeWidth="1.33333335e-11" points="0 0 23.9999985 0 23.9999985 23.9999985 0 23.9999985"></polygon>
												<path d="M18.9999989,8.99999946 L14.9999991,8.99999946 L14.9999991,2.99999982 L8.99999946,2.99999982 L8.99999946,8.99999946 L4.9999997,8.99999946 L11.9999993,15.999999 L18.9999989,8.99999946 Z M4.9999997,17.9999989 L4.9999997,19.9999988 L18.9999989,19.9999988 L18.9999989,17.9999989 L4.9999997,17.9999989 Z" id="Fill-2" fill="#227171"></path>
											</g>
										</g>
									</g>
								</svg>
							</div>
						</a>
					</h1>

					<Menu
						visitedSections={visitedSections}
					/>

					<Router />

					<div className="bottom-links">
						{backButton}
						{continueButton}
					</div>

				</div>
			);

		} else {

			return (

				<div className="container-fluid">
					<Loading />
				</div>

			);

		}

	}

}


const mapStateToProps = (state) => ({
	learning: state.embryosLearningReducer.data,
});

Learning.propTypes = {
	learning: PropTypes.object,
};

export default withRouter(connect(mapStateToProps)(Learning));
