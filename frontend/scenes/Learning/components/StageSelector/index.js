import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions } from '../../../../reducers/embryos_learning_reducer';

import './styles.scss';

class StageSelector extends Component {


	onStageItemClick(stage) {

		const clustersByStage = this.props.clustersByStage;
		const stageObject = clustersByStage[stage];

		this.props.dispatch(actions.uiSelectStage(stageObject));

	}

	getStagesList() {

		let stages = [];
		const clustersByStage = this.props.clustersByStage;

		Object.keys(clustersByStage).forEach(function (key) {
			stages.push(key);

		});

		return stages;

	}

	getStageValueFromCurrentDevelopmentStageObject() {

		return this.props.currentDevelopmentStage.classification;

	}

	render() {

		// Get Stages Values

		const stagesList = this.getStagesList();

		let currentSelectedStage = null;
		if(stagesList.length > 0) {

			// Get Current Stage Value

			currentSelectedStage = (this.props.currentDevelopmentStage !== undefined) && (Object.keys(this.props.currentDevelopmentStage).length > 0) ?
									this.getStageValueFromCurrentDevelopmentStageObject() :
									stagesList[0];
		}

		return (

				<span className="designation-num changeable">{currentSelectedStage}<div className="overflow"></div>

					<div className="options">

						{

							stagesList.map(function (value) {

								return <span key={value} onClick={this.onStageItemClick.bind(this, value)}>{value}</span>;

							}, this)

						}

					</div>

				</span>

		);

	}

}

const mapStateToProps = (state) => {

	return {
		clustersByStage: state.embryosLearningReducer.data.clustersByStage,
		currentDevelopmentStage: state.embryosLearningReducer.currentDevelopmentStage,
		currentEmbryoQuality: state.embryosLearningReducer.currentEmbryoQuality
	};

};

StageSelector.propTypes = {
	clustersByStage: PropTypes.object,
	currentDevelopmentStage: PropTypes.object,
	currentEmbryoQuality: PropTypes.object,
	dispatch: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(StageSelector);
