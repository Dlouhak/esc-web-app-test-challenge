import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
			LEARNING_PATHNAME,
			INTRODUCTION_PATHNAME,
			STAGES_OF_DEVELOPMENT_PATHNAME,
			EMBRYO_QUALITY_PATHNAME,
			EMBRYO_EVENTS_PATHNAME,
			IMPLANTATION_CHANCE_PATHNAME

		} from '../../index.js';

import NavigationLink from './components/NavigationLink';

import './styles.scss';

class Menu extends Component {

	findPathnameInVisitedSections(pathname) {

		let result = false;

		const visitedSections = this.props.visitedSections;

		if(visitedSections.indexOf(pathname) !== -1)
			result = true;

		return result;

	}

	render() {

		let introductionClassName = (this.findPathnameInVisitedSections(LEARNING_PATHNAME) || this.findPathnameInVisitedSections(INTRODUCTION_PATHNAME)) ? "done" : null;
		let stagesOfDevelopmentClassName = this.findPathnameInVisitedSections(STAGES_OF_DEVELOPMENT_PATHNAME) ? "done" : null;
		let qualityOfDivisionClassName = this.findPathnameInVisitedSections(EMBRYO_QUALITY_PATHNAME) ? "done" : null;
		let embryoEventsClassName = this.findPathnameInVisitedSections(EMBRYO_EVENTS_PATHNAME) ? "done" : null;
		let implantationChanceClassName = this.findPathnameInVisitedSections(IMPLANTATION_CHANCE_PATHNAME) ? "done" : null;

		return (

				<nav className="nav evaluation-nav">

					<ul>
						<NavigationLink to="/learning" className={introductionClassName}>Introduction</NavigationLink>
						<NavigationLink to="/learning/stages_of_development" className={stagesOfDevelopmentClassName}>Stages of development</NavigationLink>
						<NavigationLink to="/learning/embryo_quality" className={qualityOfDivisionClassName}>Quality of division</NavigationLink>
						<NavigationLink to="/learning/events" className={embryoEventsClassName}>Events</NavigationLink>
						<NavigationLink to="/learning/implantation_potential" className={implantationChanceClassName}>Implantation chance</NavigationLink>
					</ul>

				</nav>

		);

	}

}

Menu.propTypes = {
	visitedSections: PropTypes.array
};

export default Menu;
