import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class NavigationLink extends Component {
	render() {
		const isActive = this.context.router.route.location.pathname === this.props.to;
		const className = isActive ? 'active' : '';

		return(
			<li className={className}>
				<Link className={className} {...this.props}>
					{this.props.children}
				</Link>
			</li>
		);
	}
}

NavigationLink.contextTypes = {
	router: PropTypes.object
};

export default NavigationLink;
