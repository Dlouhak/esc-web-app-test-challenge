import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Introduction from '../../scenes/Introduction';
import StagesOfDevelopment from '../../scenes/StagesOfDevelopment';
import EmbryoQuality from '../../scenes/EmbryoQuality';
import Events from '../../scenes/Events';
import ImplantationPotential from '../../scenes/ImplantationPotential';


class Router extends Component {

	render() {

		return(

				<Switch>

					<Route
						exact
						path="/learning"
						component={Introduction}
					/>

					<Route
						path="/learning/introduction"
						component={Introduction}
					/>

					<Route
						path="/learning/stages_of_development"
						component={StagesOfDevelopment}
					/>

					<Route
						path="/learning/embryo_quality"
						component={EmbryoQuality}
					/>

					<Route
						path="/learning/events"
						component={Events}
					/>

					<Route
						path="/learning/implantation_potential"
						component={ImplantationPotential}
					/>

				</Switch>

		);

	}


}

export default Router;
