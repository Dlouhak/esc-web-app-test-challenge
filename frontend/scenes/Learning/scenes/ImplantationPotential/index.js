import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions } from '../../../../reducers/embryos_learning_reducer';

import EmbryosTable from '../../components/EmbryosTable';
import EmbryosTableByImplantanceChance from '../../components/EmbryosTableByImplantanceChance';

import './styles.scss';

class ImplantationPotential extends Component {

	constructor(props) {
		super(props);
		this.onChangeDisplayClick = this.onChangeDisplayClick.bind(this);
	}

	componentDidMount() {
		this.props.dispatch(actions.visitEmbryosLearning('implantationChance'));
	}

	componentWillUnmount() {
		this.props.dispatch(actions.leaveEmbryosLearning('implantationChance'));
	}

	onChangeDisplayClick() {

		const view = this.props.currentImplantanceChanceView;

		if(view === "table") {

			this.props.dispatch(actions.uiChangeViewImplantanceChance("implantation_chance"));

		} else {

			this.props.dispatch(actions.uiChangeViewImplantanceChance("table"));

		}

	}

	render() {

		const clustersByStage = this.props.clustersByStage;

		const currentTableView = this.props.currentImplantanceChanceView;
		const currentTable = currentTableView === "table" ? (
			<EmbryosTable implantationChance={true} />
		) : (
			<EmbryosTableByImplantanceChance />
		);

		const changeViewButtonText = (currentTableView === "table") ? "Arrange by implantation chance" : "Arrange by stage and quality";
		const changeDisplayButtonClass = (currentTableView === "table") ? "learning-change-display-button btn btn-transparent" : "learning-change-display-implantation-button btn btn-transparent";

		return (Object.keys(clustersByStage).length > 0) ?

			<section className="learning-main" id="implantationChance">
				<header>
					<div className="designation-box">
						<div className="designation-num active">IP</div>
					</div>

					<div className="main-desc">
						<p>Recognizing embryos with best <b>Implantation potential (IP)</b> is primary goal of grading system. When we align all clusters by <b>stages on y axis</b> and by <b>implantation potential on x</b>, we can see curve of implantation potential across the cluster</p>
					</div>
				</header>
				<section id="main-properties">
					<h2>All embryos with clusters
						<div className="desc">arranged by stages with quality and event indicators</div>
					</h2>

					<div className="inside">
						<a className={changeDisplayButtonClass} onClick={this.onChangeDisplayClick}>{changeViewButtonText}</a>
						{currentTable}
					</div>

				</section>
			</section>

		: null;

	}

}

const mapStateToProps = (state) => {

	return {

		clustersByStage: state.embryosLearningReducer.data.clustersByStage,

		currentImplantanceChanceView: state.embryosLearningReducer.currentImplantanceChanceView

	};

};

ImplantationPotential.propTypes = {

	view: PropTypes.string,

	clustersByStage: PropTypes.object,

	currentImplantanceChanceView: PropTypes.string,

	dispatch: PropTypes.func.isRequired

};

ImplantationPotential.defaultProps = {

	currentImplantanceChanceView: "table"

};


export default connect(mapStateToProps)(ImplantationPotential);
