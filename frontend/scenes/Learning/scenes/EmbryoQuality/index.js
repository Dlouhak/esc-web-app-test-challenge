import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import './styles.scss';

import EmbryoDetail from '../../../../components/EmbryoDetail';

import StageSelector from '../../components/StageSelector';
import EmbryosTable from '../../components/EmbryosTable';

import { actions } from '../../../../reducers/embryos_learning_reducer';

class EmbryoQuality extends Component {

	componentDidMount() {
		this.props.dispatch(actions.visitEmbryosLearning('embryoQuality'));
	}

	componentWillUnmount() {
		this.props.dispatch(actions.leaveEmbryosLearning('embryoQuality'));
	}

	onEmbryoQualityClick(value) {

		let selectedEmbryoQuality = null;
		const currentDevelopmentStage = this.props.currentDevelopmentStage;

		if(Object.keys(currentDevelopmentStage).length > 0) {

			const clustersByQuality = currentDevelopmentStage.clustersByQuality;

			Object.keys(clustersByQuality).forEach(function (key) {

				if(value === key)
					selectedEmbryoQuality = clustersByQuality[key];

			});

		}

		this.props.dispatch(actions.uiSelectQuality(selectedEmbryoQuality));

	}

	getStagesList() {

		let stages = [];
		const clustersByStage = this.props.clustersByStage;

		Object.keys(clustersByStage).forEach(function (key) {
			stages.push(key);

		});

		return stages;

	}


	getQualityList() {

		let result = [];
		const currentDevelopmentStage = this.props.currentDevelopmentStage;

		if(Object.keys(currentDevelopmentStage).length > 0) {

			const clustersByQuality = currentDevelopmentStage.clustersByQuality;

			Object.keys(clustersByQuality).forEach(function (key) {
				result.push(key);
			});

		}

		return result;

	}

	getCurrentQualityValue() {

		return this.props.currentEmbryoQuality ? this.props.currentEmbryoQuality.classification.substr(1) : null;

	}

	getStageValueFromCurrentDevelopmentStageObject() {

		return this.props.currentDevelopmentStage.classification;

	}

	onReferenceLineChange = (value) => {
		this.props.dispatch(actions.uiMoveCurrentQualityReferenceLine(value));
	};

	render() {

		// Clusters by Stage

		const clustersByStage = this.props.clustersByStage;

		// Current Development Stage

		const currentDevelopmentStage = this.props.currentDevelopmentStage;
		const stagesList = this.getStagesList();


		let currentSelectedStageValue = null;
		if(stagesList.length > 0) {

			currentSelectedStageValue = (currentDevelopmentStage !== undefined) && (Object.keys(currentDevelopmentStage).length > 0) ?
									this.getStageValueFromCurrentDevelopmentStageObject() :
									stagesList[0];

		}

		// Current Embryo Quality

		const currentEmbryoQuality = this.props.currentEmbryoQuality;

		// Quality List Values

		const qualityList = this.getQualityList();

		// Current Selected Stage Value

		let currentSelectedQualityValue = null;
		if(qualityList.length > 0) {

			currentSelectedQualityValue = (currentEmbryoQuality !== undefined) && (Object.keys(currentEmbryoQuality).length > 0) ?
									this.getCurrentQualityValue() :
									qualityList[0].substr(1);

		}


		return (Object.keys(clustersByStage).length > 0) ?

			<section className="learning-main">

				{/* Header */}

				<header>

					<div className="designation-box">

						<StageSelector />
						<span className="quality-value designation-num active">{currentSelectedQualityValue}</span>
						<span className="event-value designation-num">{currentSelectedStageValue < 7 ? 'A' : '\u00a0'}</span>

					</div>

					<div className="main-desc">
						<p><b>Second position</b> in cluster marking scheme represents <b>quality</b> of an embryo. Values (A,B,C,D) are assigned according to observations of cell <b>division</b> occurring within the embryo.</p>
					</div>

				</header>


				{/* Embryo Quality Main Section */}

				<section id="main-properties">

					<h2>Choose quality</h2>

					<div className="inside">

						<div className="designation-box">

							<div className="desc"><b>Quality</b> of division. We recognize up to 4 qualities marked A - D</div>

							{

								qualityList.map((value) => {

									const quality = value.substr(1);
									const qualityItemClassName = (quality === currentSelectedQualityValue) ? "designation-num quality-value active" : "designation-num quality-value";
									const cluster = this.props.currentDevelopmentStage.clustersByQuality[value];

									return <div key={value} className={qualityItemClassName} onClick={this.onEmbryoQualityClick.bind(this, value)}>
										{quality}
										<div className="img">
											<img src={cluster.embryo.last_image_url} alt={`Cluster ${value} embryo`} />
										</div>
									</div>;

								})

							}

						</div>

						{/* Chosen Quality */}

						<div className="designation-chosen">
							<div className="designation-num">
								<span>{currentSelectedStageValue}</span>
								<span className="active-color quality-value">{currentSelectedQualityValue}</span>
							</div>
						</div>

						<h3>
							{currentSelectedStageValue < 7 ? currentDevelopmentStage.name : currentEmbryoQuality.name}
							{currentSelectedStageValue < 7 && ' '}
							{currentSelectedStageValue < 7 && <span className="active-color">
								{currentEmbryoQuality.name.substr(currentDevelopmentStage.name.length + 1)}
							</span>}
							<div className="desc">
								{currentDevelopmentStage.shortName}
								{' '}
								<span className="active-color">{currentEmbryoQuality.shortName.substr(currentDevelopmentStage.shortName.length + 1)}</span>
							</div>
						</h3>

						<p>{currentEmbryoQuality.description}</p>

					</div>

					{currentEmbryoQuality.embryo && <div className="learning-embryo-graph-container">

						<EmbryoDetail
							embryo={currentEmbryoQuality.embryo}
							current={this.props.currentQualityPlayPosition}
							onReferenceLineChange={this.onReferenceLineChange}
						/>

					</div>}

					<hr className="learning-divider" />

					<h2 className="learning-table-title">All embryos
						<div className="desc">arranged by stage and quality</div>
					</h2>

					<div className="inside">

						<EmbryosTable events={false} clusterClickable={false} />

					</div>

				</section>

			</section>

		: null;

	}

}

const mapStateToProps = (state) => {

	return {

		clustersByStage: state.embryosLearningReducer.data.clustersByStage,

		currentDevelopmentStage: state.embryosLearningReducer.currentDevelopmentStage,
		currentEmbryoQuality: state.embryosLearningReducer.currentEmbryoQuality,
		currentQualityPlayPosition: state.embryosLearningReducer.currentQualityPlayPosition

	};

};

EmbryoQuality.propTypes = {

	clustersByStage: PropTypes.object,

	currentDevelopmentStage: PropTypes.object,
	currentEmbryoQuality: PropTypes.object,
	currentQualityPlayPosition: PropTypes.number,

	dispatch: PropTypes.func.isRequired

};

EmbryoQuality.defaultProps = {

	clustersByStage: {},

	currentDevelopmentStage: {},
	currentEmbryoQuality: {}

};

export default connect(mapStateToProps)(EmbryoQuality);
