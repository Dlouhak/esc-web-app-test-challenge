import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TreeView from 'react-treeview';
import 'react-treeview/react-treeview.css';

import { actions } from '../../../../reducers/embryos_library_reducer';

import './styles.scss';


class EmbryosFilter extends Component {

	isCategorySelected(category) {
		const { embryosByCategories, currentFilteredEmbryos } = this.props;

		return currentFilteredEmbryos !== null &&
			(currentFilteredEmbryos[category] || []).length === embryosByCategories[category].length;
	}

	isClusterSelected(category, cluster) {
		const { currentFilteredEmbryos } = this.props;
		return currentFilteredEmbryos !== null && (currentFilteredEmbryos[category] || []).indexOf(cluster) >= 0;
	}

	onNodeClick(category) {
		if (this.isCategorySelected(category)) {
			this.props.dispatch(actions.uiUnselectEmbryoNodeFilter(category));
		} else {
			const clusters = this.props.embryosByCategories[category].map(({ cluster })=> cluster);
			this.props.dispatch(actions.uiSelectEmbryoNodeFilter(category, clusters));
		}
	}

	onEntryClick(category, cluster) {
		if (this.isClusterSelected(category, cluster)) {
			this.props.dispatch(actions.uiUnselectEmbryoEntryFilter(category, cluster));
		} else {
			this.props.dispatch(actions.uiSelectEmbryoEntryFilter(category, cluster));
		}
	}

	render() {
		const { embryosByCategories } = this.props;

		return (
			<div id="sidebar-left">
				<div className="Filter-container">
					<div className="node sidebar-header">Phases of development</div>
					<div className="sexy-checkbox-radio-wrapper">
						{Object.keys(embryosByCategories).map((category) => {
							const categoryHtmlId = `EmbryosFilter-category[${category}]`;
							const categoryLabel = (
								<div>
									<div className="node" onClick={this.onNodeClick.bind(this, category)}>
										{category}

										<div className="checkbox checkbox-primary checkbox-node">
											<input
												id={categoryHtmlId}
												type="checkbox"
												checked={this.isCategorySelected(category)}
											/>
											<label htmlFor={categoryHtmlId} />
										</div>
									</div>
								</div>
							);

							return (
								<div key={category}>
									<TreeView nodeLabel={categoryLabel} defaultCollapsed={false}>
										{embryosByCategories[category].map((cluster) => {
											const clusterHtmlId = `EmbryosFilter-cluster[${cluster.cluster}]`;
											const clusterNameEdited = cluster.learning_name.replace(/([^<])\/([^>])/g, "$1 / $2");

											return (
												<div key={cluster.cluster}>
													<div
														className={`info cluster-default-bg-color cluster-bg-color-${cluster.cluster}`}
														onClick={this.onEntryClick.bind(this, category, cluster.cluster)}
													>
														<span className="l-h tooltip-top tooltip-top__large tooltip-netvor" data-tooltip-large data-tooltip={clusterNameEdited}>
															{cluster.cluster}{' '}{cluster.implantation_chance}%
														</span>
													</div>

													<div className="checkbox checkbox-primary">
														<input
															type="checkbox"
															id={clusterHtmlId}
															checked={this.isClusterSelected(category, cluster.cluster)}
															onChange={this.onEntryClick.bind(this, category, cluster.cluster)}
														/>

														<label htmlFor={clusterHtmlId} />
													</div>
												</div>
											);
										})}
									</TreeView>
								</div>
							);
						})}
					</div>
				</div>
			</div>
		);
	}

}

EmbryosFilter.propTypes = {
	embryosByCategories: PropTypes.object.isRequired,
	currentFilteredEmbryos: PropTypes.object,
	dispatch: PropTypes.func.isRequired,
};

EmbryosFilter.defaultProps = {
	currentFilteredEmbryos: null,
};


export default connect()(EmbryosFilter);
