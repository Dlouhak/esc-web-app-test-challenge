import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';


class TopBar extends PureComponent {

	onClickEmbryosGridView = () => {
		this.props.toggleListView(false);
	};

	onClickEmbryosListView = () => {
		this.props.toggleListView(true);
	};

	render() {
		const viewMoreStrokeColor = !this.props.listView ? "#F47F36" : "#B2B2B2";
		const viewLessStrokeColor = this.props.listView ? "#F47F36"  : "#B2B2B2";

		return (
			<div className="container-fluid TopBar">
				<div className="row">
					<div className="col-md-6">
						<div className="title-container">
							<h1 className="embryos-grid-title">Overview</h1>
						</div>
					</div>
					<div className="col-md-6 my-auto">
						<div className="float-right">
							<p className="embryos-grid-top-bar-text Topbar-paragraph">displaying <span>{this.props.shownEmbryosCounter}</span> embryos</p>

							{/* View More Icon */}
							<div className="embryos-grid-list-view-toggle" onClick={this.onClickEmbryosGridView}>
								<svg width="23px" height="22px" viewBox="0 0 23 22">
									<g id="Guidelines" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
										<g id="Icons" transform="translate(-670.000000, -161.000000)" stroke={viewMoreStrokeColor} strokeWidth="2">
											<g id="icon-viewmore-normal" transform="translate(671.000000, 162.000000)">
												<rect id="Rectangle-path" x="0" y="0" width="9" height="8" rx="2" />
												<rect id="Rectangle-path-Copy" x="12" y="0" width="9" height="8" rx="2" />
												<rect id="Rectangle-path-Copy-3" x="0" y="12" width="9" height="8" rx="2" />
												<rect id="Rectangle-path-Copy-2" x="12" y="12" width="9" height="8" rx="2" />
											</g>
										</g>
									</g>
								</svg>
							</div>

							{/* View Less Icon */}
							<div className="embryos-grid-list-view-toggle" onClick={this.onClickEmbryosListView}>
								<svg width="22px" height="22px" viewBox="0 0 22 22">
									<g id="Guidelines" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
										<g id="Icons" transform="translate(-671.000000, -210.000000)" strokeWidth="2" stroke={viewLessStrokeColor}>
											<g id="icon-viewless-normal" transform="translate(672.000000, 211.000000)">
												<g id="server">
													<rect id="Rectangle-path" x="0" y="0" width="20" height="8" rx="2" />
													<rect id="Rectangle-path" x="0" y="12" width="20" height="8" rx="2" />
												</g>
											</g>
										</g>
									</g>
								</svg>
							</div>

							{/* View Download Icon */}
							<a href={`${process.env.PUBLIC_URL}/api/csv`} className="embryos-grid-list-view-toggle last-icon">
								<svg width="20px" height="22px" viewBox="0 0 20 22">
									<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-672.000000, -257.000000)" strokeLinecap="round" strokeLinejoin="round">
										<g id="icon-download-normal" transform="translate(673.000000, 258.000000)" stroke="#B2B2B2" strokeWidth="2">
											<title>Download CSV</title>
											<path d="M0,15 L0,18 C0,19.1045695 0.8954305,20 2,20 L16,20 C17.1045695,20 18,19.1045695 18,18 L18,15" id="Shape" />
											<polyline id="Shape" points="5 10 9 14 13 10" />
											<path d="M9,0 L9,14" id="Shape" />
										</g>
									</g>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}

}

TopBar.propTypes = {
	shownEmbryosCounter: PropTypes.number.isRequired,
	listView: PropTypes.bool.isRequired,
	toggleListView: PropTypes.func.isRequired,
};


export default TopBar;
