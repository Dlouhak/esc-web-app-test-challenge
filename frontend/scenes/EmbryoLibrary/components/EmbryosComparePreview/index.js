import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { actions } from '../../../../reducers/embryos_library_reducer';

import iconReady from '../../../../img/icon-comparetwo.svg';
import iconNotReady from '../../../../img/icon-compareone.svg';
import './styles.scss';


class EmbryosComparePreview extends Component {

	onRemoveFirstEmbryoClick = () => {
		this.props.dispatch(actions.uiRemoveFromCompareFirstEmbryo());
	};

	onRemoveSecondEmbryoClick = () => {
		this.props.dispatch(actions.uiRemoveFromCompareSecondEmbryo());
	};

	onCloseClick = () => {
		if (this.props.isCompareCompleteVisible) {
			// Compare is currently visible, then hide it
			this.props.dispatch(actions.uiCompareCollapse());
		} else {
			// Compare is currently collapsed, then show it
			this.props.dispatch(actions.uiCompareShow());
		}
	};

	render() {
		const currentCompareFirstEmbryo = this.props.currentCompareFirstEmbryo;
		const currentCompareFirstEmbryoEmpty = Object.keys(currentCompareFirstEmbryo).length === 0;

		const currentCompareSecondEmbryo = this.props.currentCompareSecondEmbryo;
		const currentCompareSecondEmbryoEmpty = Object.keys(currentCompareSecondEmbryo).length === 0;

		if (currentCompareFirstEmbryoEmpty && currentCompareSecondEmbryoEmpty) {
			return null;
		}

		const compareReady = !currentCompareFirstEmbryoEmpty && !currentCompareSecondEmbryoEmpty;

		const embryosCompareClasses = this.props.isCompareCompleteVisible ? 'col-12 fixed-bottom' : 'col-12 fixed-bottom hide-compare';
		const buttonClass = compareReady ? 'btn btn-compare compare-ready' : 'btn btn-compare disabled-link';
		const hiddenBarText = compareReady ? 'Ready to compare' : 'You need select one more embryo to compare';

		const compareInformation = compareReady ? (
			<div className="compare-information ready">
				<img src={iconReady} alt="Ready to compare" />Ready to compare
			</div>
		) : (
			<div className="compare-information">
				<img src={iconNotReady} alt="You need select one more embryo to compare" />Select 1 more
			</div>
		);

		const firstEmbryoImg = currentCompareFirstEmbryoEmpty ? (
			<div className="First-embryo compare-embryo">
				<div className="no-compare-image"><div /><span>Select Embryo</span></div>
			</div>
		) : (
			<div className="First-embryo compare-embryo">
				<div className="info-wrap d-inline-block">
					<p className="info">
						<span className="l-h tooltip-top tooltip-top__large tooltip-netvor" data-tooltip-large="true" data-tooltip={this.props.currentCompareFirstCategory.learning_name || null}>
							{this.props.currentCompareFirstEmbryo.cluster}
						</span>
					</p>
					<p className="cluster-id">
						{this.props.currentCompareFirstEmbryo.id}
					</p>
				</div>
				<div className="d-inline-block Compare-image">
					<img
						className="d-inline-block"
						src={this.props.currentCompareFirstEmbryo.last_image_url}
						alt={this.props.currentCompareFirstEmbryo.cluster}
					/>
					<a className="remove-compare-embryo" onClick={this.onRemoveFirstEmbryoClick}>Remove</a>
				</div>
			</div>
		);

		const secondEmbryoImg = currentCompareSecondEmbryoEmpty ? (
			<div className="Second-embryo compare-embryo">
				<div className="no-compare-image"><div /><span>Select Embryo</span></div>
			</div>
		) : (
			<div className="Second-embryo compare-embryo">
				<div className="d-inline-block Compare-image">
					<img
						className="d-inline-block"
						src={this.props.currentCompareSecondEmbryo.last_image_url}
						alt={this.props.currentCompareSecondEmbryo.cluster}
					/>
					<a className="remove-compare-embryo" onClick={this.onRemoveSecondEmbryoClick}>Remove</a>
				</div>
				<div className="info-wrap d-inline-block">
					<p className="info">
						<span className="l-h tooltip-top tooltip-top__large tooltip-netvor" data-tooltip-large="true" data-tooltip={this.props.currentCompareSecondCategory.learning_name || null}>
							{this.props.currentCompareSecondEmbryo.cluster}
						</span>
					</p>
					<p className="cluster-id">
						{this.props.currentCompareSecondEmbryo.id}
					</p>
				</div>
			</div>
		);

		return (
			<div className={embryosCompareClasses} id="embryos-compare">
				<div className="row">
					<div className="col-12 col-md-2">
						{compareInformation}
					</div>

					<div className="col-12 col-md-3 no-padding">
						{firstEmbryoImg}
					</div>

					<div className="col-12 col-md-3 no-padding">
						<div className="text-center d-block h-100">
							<div className="compare-btn-wrapper">
								<Link
									className={buttonClass}
									to={`/embryos_compare/${currentCompareFirstEmbryo.id}/${currentCompareSecondEmbryo.id}`}
								>
									Compare
								</Link>
							</div>
						</div>
					</div>

					<div className="col-12 col-md-4 no-padding">
						{secondEmbryoImg}
					</div>

					<a className="close" onClick={this.onCloseClick}>Close</a>
					<span className="position-absolute text-center">{hiddenBarText}</span>
				</div>
			</div>
		);
	}

}

EmbryosComparePreview.propTypes = {
	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareFirstCategory: PropTypes.object.isRequired,

	currentCompareSecondEmbryo: PropTypes.object.isRequired,
	currentCompareSecondCategory: PropTypes.object.isRequired,

	isCompareCompleteVisible: PropTypes.bool.isRequired,

	dispatch: PropTypes.func.isRequired,
};


export default connect()(EmbryosComparePreview);
