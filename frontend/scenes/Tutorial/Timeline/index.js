import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactResizeDetector from 'react-resize-detector';
import range from 'lodash/range';

import { moveReferenceLine } from '../../../actions/tutorialActions';
import { FRAMES_COUNT, STEP } from '../EmbryoImages';
import './index.scss';


const STEPS_BETWEEN_BIG_STEEP = 20; // 10h => 10 * 60 / 30

function formatTime(step) {
	const minutes = step * 30;
	const hours = Math.floor(minutes / 60);
	return `${hours}:${(minutes % 60) === 0 ? '00' : '30'}`;
}


class Timeline extends Component {

	static propTypes = {
		color: PropTypes.string,
		leftOffset: PropTypes.number,
		stepsCount: PropTypes.number,
		embryo: PropTypes.object,
		selectedReferenceLine: PropTypes.number.isRequired,
		onMoveReferenceLine: PropTypes.func.isRequired,
	};

	static defaultProps = {
		color: '#7F7F7F',
		leftOffset: 120,
		stepsCount: 200,
		embryo: null,
	};

	state = {
		width: -1,
		height: -1,
	};

	handleResize = (width, height) => {
		this.setState({ width, height });
	};

	handleReferenceLineMove = (e) => {
		this.props.onMoveReferenceLine(+e.target.getAttribute('data-line'));
	};

	render() {
		const { width, height } = this.state;

		if (width < 0 || height < 0) {
			return <ReactResizeDetector handleWidth handleHeight onResize={this.handleResize} />;
		}

		const { color, leftOffset, selectedReferenceLine, embryo } = this.props;

		const stepsCount = embryo ?
			Math.round(embryo.image_offset_minutes / STEP) + (+embryo.image_count) - 1 :
			this.props.stepsCount;

		const xStep = width / stepsCount;
		const maxHeightOfTimeline = height - 45;
		const minHeightOfTimeline = height - 18;

		const widthOfFrame = width / FRAMES_COUNT;
		const topOffset = embryo ? widthOfFrame : 0;

		const heightOfRect = 22;
		const widthOfRect = embryo ? widthOfFrame : 50;
		const halfWidthOfRect = widthOfRect / 2;

		const heightBetweenStairs = [43, 39, 31, 26, 23, 20];
		const heightOfStairs = {};
		for (let i = 0; i < heightBetweenStairs.length; i++) {
			const heightOfStair = height - heightBetweenStairs[i];
			heightOfStairs[selectedReferenceLine - i - 1] = heightOfStair;
			heightOfStairs[selectedReferenceLine + i + 1] = heightOfStair;
		}

		const heightOfStartingStairs = [5, 11, 14, 16, 17].map(val => height - val);

		return (
			<Fragment>
				<ReactResizeDetector handleWidth handleHeight onResize={this.handleResize} />
				{width >= 0 && height >= 0 ? (
					<div className="timeline">
						<svg width={width} height={height}>
							{range(stepsCount).map(i => {
								const x = (i * xStep) + leftOffset;
								const isThisLineSelected = i === selectedReferenceLine;
								const hasTime = (i % STEPS_BETWEEN_BIG_STEEP) === 0 &&
									!isThisLineSelected &&
									!(i in heightOfStairs);

								const distanceToEnd = width - x;

								const rectLeftOffset = x < halfWidthOfRect
									? x
									: Math.max(halfWidthOfRect, widthOfRect - distanceToEnd);

								const textLeftOffset = x < halfWidthOfRect
									? halfWidthOfRect - x
									: Math.min(distanceToEnd - halfWidthOfRect, 0);

								const tickHeight = heightOfStairs[i] || (isThisLineSelected ?
									maxHeightOfTimeline :
									(heightOfStartingStairs[i] || minHeightOfTimeline)
								);

								return (
									<Fragment key={i}>
										{isThisLineSelected ? (
											<g>
												<rect
													data-line={i}
													x={x - rectLeftOffset}
													y={topOffset}
													width={widthOfRect}
													height={heightOfRect}
													fill="#FA6A6A"
												/>
												<text
													x={x + textLeftOffset}
													y={topOffset + 15}
													fill="#fff"
													fontSize={12}
													style={{ textAnchor: 'middle' }}
												>
													{formatTime(i)}
												</text>
											</g>
										) : null}

										<line // Reference line
											data-line={i}
											onMouseEnter={this.handleReferenceLineMove}
											x1={x}
											y1={height - 50}
											x2={x}
											y2={topOffset + heightOfRect}
											stroke="#FA6A6A"
											strokeWidth={2}
											style={{ opacity: isThisLineSelected ? 1 : 0 }}
										/>

										<line // Timeline
											data-line={i}
											x1={x}
											y1={height}
											x2={x}
											y2={tickHeight}
											stroke={color}
											strokeWidth={1}
										/>

										{hasTime ? (
											<text
												x={x}
												y={minHeightOfTimeline - 10}
												fill={color}
												fontSize={10}
												style={{ textAnchor: leftOffset === 0 ? 'start' : 'middle' }}
											>
												{formatTime(i)}
											</text>
										) : null}
									</Fragment>
								);
							})}
						</svg>
					</div>
				) : null}
			</Fragment>
		);
	}

}


const mapStateToProps = (state) => ({
	selectedReferenceLine: state.embryosTutorialReducer.selectedReferenceLine,
});

const mapDispatchToProps = (dispatch) => ({
	onMoveReferenceLine: (line) => dispatch(moveReferenceLine(line)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Timeline);
