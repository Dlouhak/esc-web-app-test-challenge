import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


export default class Clusters extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content" id="clusters">
				<div className="head">
					<h1>Clusters</h1>
					<p>Clusters are technically sets of embryos with similar characteristics. The semantics of the cluster definition is explained in the visual. Clusters are generally logically ordered from 1 to 8 where the best ones are approaching 1 (with subsets [AB,BA,CA etc.] that give more specific information) and vice versa.</p>
				</div>
				<hr />

				<div className="box">
					<div className="row-item">
						<span className="designation-num active">1</span>
						<span className="designation-num">a</span>
						<span className="designation-num">a</span>
						<div className="p">
							<p>Number, which is in <b>the first position of the CATI grading system</b>, represents a degree of <b>embryo's expansion</b> of blastocyst cavity and its progress in hatching out of the zona pellucida on the scale from 1 to 8.</p>
						</div>
					</div>

					<div className="row-item">
						<span className="designation-num">1</span>
						<span className="designation-num active">a</span>
						<span className="designation-num">a</span>
						<div className="p">
							<p><b>Second position</b> in cluster marking scheme represents <b>quality</b> of an embryo. Values (A,B,C,D) are assigned according to observations of cell <b>division</b> occurring within the embryo.</p>
						</div>
					</div>

					<div className="row-item">
						<span className="designation-num">1</span>
						<span className="designation-num">a</span>
						<span className="designation-num active">a</span>
						<div className="p">
							<p><b>Third position</b> of cluster mark used in ESCape is <b>event timing</b> of embryo. It is subcathegory of quality of embryo.</p>
						</div>
					</div>
				</div>
			</div>
		);
	}

}
