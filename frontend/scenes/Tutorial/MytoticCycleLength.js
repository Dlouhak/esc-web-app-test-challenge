import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Timeline from './Timeline';


export default class MytoticCycleLength extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head">
					<h1>Length of mytotic cycle</h1>
					<p>Due to the technical limitations that we face, we have decided to observeonly the fastest lineage of mytosis. Given the data, we always look for the fork that divides first. The reason why we do this is (aside of technical limitations) that we expect the best possible scenario for every embryo given the circumstances. Meaning that if the fastest gen. lineage is fast enough, we give it a try no matter how the rest of cell divisions slow down.</p>
				</div>

				<div className="full">
					<hr />
					<div className="wrap">
						<div className="zero column">
							<div>
								<div className="bub bub-w high-line">
									M0
								</div>
							</div>
						</div>

						<div className="column">
							<div>
								<div className="bub bub-g high-line">
									M1.1
									<span />
								</div>
								<div className="bub bub-g">
									M1.2
								</div>
							</div>
						</div>

						<div className="column double">
							<div>
								<div className="bub bub-b high-line">
									M2.1
									<span />
								</div>
								<div className="bub bub-b">
									M2.2
								</div>
							</div>
							<div>
								<div className="bub bub-b">
									M2.3
								</div>
								<div className="bub bub-b">
									M2.4
								</div>
							</div>
						</div>

						<div className="column double half">
							<div>
								<div className="bub bub-y high-line">
									M3.1
									<span />
								</div>
								<div className="bub bub-y">
									M3.2
								</div>
							</div>
							<div>
								<div className="bub bub-y">
									M3.3
								</div>
								<div className="bub bub-y">
									M3.4
								</div>
							</div>
						</div>
					</div>

					<Timeline stepsCount={100} />
				</div>
			</div>
		);
	}

}
