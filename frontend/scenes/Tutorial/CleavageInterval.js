import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Timeline from './Timeline';


export default class CleavageInterval extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head fix-w">
					<h1>Cleavage interval</h1>
					<p>Cleavage interval is the time that passes by between the first division in the given generationand the last division in the given generation. Cleavage interval is important because it shows how much the embryo is asynchronous in its development. The greater the lag in given generation, the lower the quality of an embryo.</p>
				</div>

				<div className="full">
					<hr />
					<div className="wrap">
						<div className="zero column">
							<div>
								<div className="bub bub-w">
									M0
								</div>
							</div>
						</div>

						<div className="column">
							<div>
								<div className="bub bub-g">
									M1.1
								</div>
								<div className="bub bub-g">
									M1.2
								</div>
							</div>
						</div>

						<div className="column double">
							<div>
								<div className="bub bub-b">
									M2.1
								</div>
								<div className="bub bub-b">
									M2.2
								</div>
							</div>
							<div>
								<div className="bub bub-b">
									M2.3
								</div>
								<div className="bub bub-b">
									M2.4
								</div>
							</div>
							<div className="highlighting clein" data-highlight="Cleavage interval" />
						</div>
					</div>

					<Timeline stepsCount={80} />
				</div>
			</div>
		);
	}

}
