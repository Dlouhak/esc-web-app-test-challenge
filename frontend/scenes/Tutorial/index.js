import React, { Component } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import 'jquery';
import 'bootstrap';

import Home from './Home';
import MytoticCycle from './MytoticCycle';
import MytoticCycleLength from './MytoticCycleLength';
import CleavageInterval from './CleavageInterval';
import TimingOfCleavage from './TimingOfCleavage';
import Clusters from './Clusters';
import ImplantationPotential from './ImplantationPotential';
import SegmentAndActivityCurve from './SegmentAndActivityCurve';
import CollapsesAndExpansion from './CollapsesAndExpansion';
import ShowcaseOnRealData from './ShowcaseOnRealData';
import Registration from './Registration';

import TypeSelectBox from './TypeSelectBox';

import logo from '../../img/icon-esclogo-white.svg';
import './index.scss';


export const PAGE_HOME = '/';
export const PAGE_MYTOTIC_CYCLE = '/mytotic-cycle';
export const PAGE_MYTOTIC_CYCLE_LENGTH = '/mytotic-cycle-length';
export const PAGE_CLEAVAGE_INTERVAL = '/cleavage-interval';
export const PAGE_TIMING_OF_CLEAVAGE = '/timing-of-cleavage';
export const PAGE_CLUSTERS = '/clusters';
export const PAGE_IMPLANTATION_POTENTIAL = '/implantation-potential';
export const PAGE_SEGMENT_AND_ACTIVITY_CURVE = '/segment-and-activity-curve';
export const PAGE_COLLAPSES_AND_EXPANSION = '/collapses-and-expansion';
export const PAGE_SHOWCASE_ON_REAL_DATA = '/showcase-on-real-data';


export default class Tutorial extends Component {

	state = {
		visitedPages: {
			[PAGE_HOME]: false,
			[PAGE_MYTOTIC_CYCLE]: false,
			[PAGE_MYTOTIC_CYCLE_LENGTH]: false,
			[PAGE_CLEAVAGE_INTERVAL]: false,
			[PAGE_TIMING_OF_CLEAVAGE]: false,
			[PAGE_CLUSTERS]: false,
			[PAGE_IMPLANTATION_POTENTIAL]: false,
			[PAGE_SEGMENT_AND_ACTIVITY_CURVE]: false,
			[PAGE_COLLAPSES_AND_EXPANSION]: false,
			[PAGE_SHOWCASE_ON_REAL_DATA]: false,
		},
	};

	setPageAsVisited = (page) => {
		this.setState({
			...this.state,
			visitedPages: {
				...this.state.visitedPages,
				[page]: true,
			},
		});
	};

	getVisitedClassName(page) {
		return this.state.visitedPages[page] === true ? 'visited' : '';
	};

	render() {
		const pages = this.state.visitedPages;
		const countVisitedPages = Object.keys(pages).map(page => pages[page]).filter(visited => visited).length;
		const visitedPagesPercent = (countVisitedPages / Object.keys(pages).length).toFixed(2) * 100;

		return (
			<div className="row">
				<div className="left-col col-md-down-12">
					<div className="head">
						<div className="logo-wrap">
							<img src={logo} alt="logo" width="82.37" height="44.68" />
							<div><strong>ESC</strong> tutorial</div>
						</div>
						<div className="progress-bar"><span style={{ right: `${100 - visitedPagesPercent}%`}} /></div>
						<div className="progress-desc">YOUR PROGRESS</div>
					</div>

					<nav className="navigation">
						<div className="nav-title">Part 1</div>
						<NavLink to={PAGE_HOME} exact={true} activeClassName="active" className={this.getVisitedClassName(PAGE_HOME)}>How does it work?</NavLink>
						<NavLink to={PAGE_MYTOTIC_CYCLE} activeClassName="active" className={this.getVisitedClassName(PAGE_MYTOTIC_CYCLE)}>Mytotic cycle</NavLink>
						<NavLink to={PAGE_MYTOTIC_CYCLE_LENGTH} activeClassName="active" className={this.getVisitedClassName(PAGE_MYTOTIC_CYCLE_LENGTH)}>Length of mytotic cycle</NavLink>
						<NavLink to={PAGE_CLEAVAGE_INTERVAL} activeClassName="active" className={this.getVisitedClassName(PAGE_CLEAVAGE_INTERVAL)}>Cleavage interval</NavLink>
						<NavLink to={PAGE_TIMING_OF_CLEAVAGE} activeClassName="active" className={this.getVisitedClassName(PAGE_TIMING_OF_CLEAVAGE)}>Timing of cleavage</NavLink>

						<div className="nav-title">Part 2</div>
						<NavLink to={PAGE_CLUSTERS} activeClassName="active" className={this.getVisitedClassName(PAGE_CLUSTERS)}>Clusters</NavLink>
						<NavLink to={PAGE_IMPLANTATION_POTENTIAL} activeClassName="active" className={this.getVisitedClassName(PAGE_IMPLANTATION_POTENTIAL)}>Implantation potential</NavLink>
						<NavLink to={PAGE_SEGMENT_AND_ACTIVITY_CURVE} activeClassName="active" className={this.getVisitedClassName(PAGE_SEGMENT_AND_ACTIVITY_CURVE)}>Segment. and activity curve</NavLink>
						<NavLink to={PAGE_COLLAPSES_AND_EXPANSION} activeClassName="active" className={this.getVisitedClassName(PAGE_COLLAPSES_AND_EXPANSION)}>Collapses and expansion</NavLink>

						<div className="nav-title">Part 3</div>
						<NavLink to={PAGE_SHOWCASE_ON_REAL_DATA} activeClassName="active" className={this.getVisitedClassName(PAGE_SHOWCASE_ON_REAL_DATA)}>Showcase on real data</NavLink>
						<Switch>
							<Route path={PAGE_SHOWCASE_ON_REAL_DATA} component={TypeSelectBox} />
						</Switch>
					</nav>
				</div>

				<Switch>
					<Route
						exact
						path={PAGE_HOME}
						render={() => <Home onVisitPage={() => this.setPageAsVisited(PAGE_HOME)} />}
					/>
					<Route
						path={PAGE_MYTOTIC_CYCLE}
						render={() => <MytoticCycle onVisitPage={() => this.setPageAsVisited(PAGE_MYTOTIC_CYCLE)} />}
					/>
					<Route
						path={PAGE_MYTOTIC_CYCLE_LENGTH}
						render={() => <MytoticCycleLength onVisitPage={() => this.setPageAsVisited(PAGE_MYTOTIC_CYCLE_LENGTH)} />}
					/>
					<Route
						path={PAGE_CLEAVAGE_INTERVAL}
						render={() => <CleavageInterval onVisitPage={() => this.setPageAsVisited(PAGE_CLEAVAGE_INTERVAL)} />}
					/>
					<Route
						path={PAGE_TIMING_OF_CLEAVAGE}
						render={() => <TimingOfCleavage onVisitPage={() => this.setPageAsVisited(PAGE_TIMING_OF_CLEAVAGE)} />}
					/>
					<Route
						path={PAGE_CLUSTERS}
						render={() => <Clusters onVisitPage={() => this.setPageAsVisited(PAGE_CLUSTERS)} />}
					/>
					<Route
						path={PAGE_IMPLANTATION_POTENTIAL}
						render={() => <ImplantationPotential onVisitPage={() => this.setPageAsVisited(PAGE_IMPLANTATION_POTENTIAL)} />}
					/>
					<Route
						path={PAGE_SEGMENT_AND_ACTIVITY_CURVE}
						render={() => <SegmentAndActivityCurve onVisitPage={() => this.setPageAsVisited(PAGE_SEGMENT_AND_ACTIVITY_CURVE)} />}
					/>
					<Route
						path={PAGE_COLLAPSES_AND_EXPANSION}
						render={() => <CollapsesAndExpansion onVisitPage={() => this.setPageAsVisited(PAGE_COLLAPSES_AND_EXPANSION)} />}
					/>
					<Route
						path={PAGE_SHOWCASE_ON_REAL_DATA}
						render={() => <ShowcaseOnRealData onVisitPage={() => this.setPageAsVisited(PAGE_SHOWCASE_ON_REAL_DATA)} />}
					/>
					<Route path="/registration" component={Registration} />
				</Switch>
			</div>
		);
	}

}
