import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { fetchEmbryoDetail } from '../../services/api';

import GraphIconOne from '../../components/GraphIconOne';
import GraphIconTwo from '../../components/GraphIconTwo';
import EmbryoImages from './EmbryoImages';
import Timeline from './Timeline';


export default class SegmentAndActivityCurve extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	state = {
		embryo: null,
	};

	componentDidMount() {
		this.props.onVisitPage();

		fetchEmbryoDetail(4432).then(data => {
			this.setState({
				...this.state,
				embryo: data.embryo,
			})
		});
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head">
					<h1>Segmentation and activity curve</h1>
					<div className="box-group links">
						<div className="sh-box info-wrap active">
							<GraphIconOne />
							<div className="info-text-wrapper">
								<p className="heading">
									Segmentation curve
								</p>
								<p className="text">
									Expression of <span className="bolder-text">embryo growth</span> – area of embryo in pixels
								</p>
							</div>
						</div>

						<div className="sh-box info-wrap">
							<GraphIconTwo />
							<div className="info-text-wrapper">
								<p className="heading">
									Activity curve
								</p>
								<p className="text">
									Expression of the <span className="bolder-text">cell changes</span> in time – correlates with the cell cleavages
								</p>
							</div>
						</div>
					</div>
				</div>

				<div className="full">
					{this.state.embryo ? (
						<EmbryoImages embryo={this.state.embryo} />
					) : null}

					<Timeline
						embryo={this.state.embryo}
						leftOffset={0}
					/>
				</div>
			</div>
		);
	}
}
