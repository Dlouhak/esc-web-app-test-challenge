import React, { PureComponent } from 'react';

import Legend from '../Legend';
import './index.scss';


export default class TypeSelectBox extends PureComponent {

	render() {
		return (
			<div className="secondary">
				<hr />
				<ul className="checkbox-area">
					<li>
						<input type="checkbox" id="cleavages" defaultChecked />
						<label htmlFor="cleavages"><span className="checkbox" />Cleavages</label>

						<ul>
							<li>
								<input type="checkbox" id="abnormal-cleavages" />
								<label htmlFor="abnormal-cleavages"><span className="checkbox" />abnormal cleavages</label>
							</li>

							<li>
								<input type="checkbox" id="time-of-cleavages" />
								<label htmlFor="time-of-cleavages"><span className="checkbox" />time of cleavages</label>
							</li>
						</ul>
					</li>

					<li>
						<input type="checkbox" id="collapses" />
						<label htmlFor="collapses"><span className="checkbox" />Collapses</label>
					</li>

					<li>
						<input type="checkbox" id="expansion-and-compaction" />
						<label htmlFor="expansion-and-compaction"><span className="checkbox" />Expansion and compaction</label>

						<ul>
							<li>
								<input type="checkbox" id="interval" />
								<label htmlFor="interval"><span className="checkbox" />Interval</label>
							</li>
						</ul>
					</li>

					<li>
						<input type="checkbox" id="mytotic-cycle" />
						<label htmlFor="mytotic-cycle"><span className="checkbox" />Mytotic cycle</label>
					</li>
				</ul>

				<Legend />
			</div>
		);
	}

}
