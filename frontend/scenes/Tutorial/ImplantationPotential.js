import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import EmbryoTable from './EmbryoTable';


export default class ImplantationPotential extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content">
				<div className="head">
					<h1>Implantation potential</h1>
					<p>As we mentioned in the "Clusters" section we order clusters by implantation potential. That means the probability with which the embryos will be successfully transferred and the receiver will become pregnant. The table shows the actual value for implantation chance for given clusters.</p>
				</div>

				<div className="full">
					<hr />
					<EmbryoTable />
				</div>
			</div>
		);
	}

}
