import React, { Component } from 'react';

import Router from './components/Router';

import './styles.scss';


class Training extends Component {

	render() {

		return (

			<div className="training-container">
				<Router />
			</div>

		);

	}

}

export default Training;
