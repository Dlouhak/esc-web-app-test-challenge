import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import EmbryoDetail from '../../../../components/EmbryoDetail';
import ArrowRight from '../../../../components/ArrowRight';
import EmbryoTrainingIcon from '../../../../components/EmbryoTrainingIcon';
import GraphUpArrow from '../../../../components/GraphUpArrow';

import Loading from '../../../../components/Loading';

import TrainingQuestionOptions from '../../components/TrainingQuestionOptions';
import EmbryoShowcase from '../../components/EmbryoShowcase';

import { actions } from '../../../../reducers/embryos_training_reducer';

import './styles.scss';

class EmbryoStageSelection extends Component {

	constructor(props) {
		super(props);
		this.state = { animationHoverSeen: false };
	}

	componentDidMount() {

		const nextLevel = this.props.nextLevel !== null ? this.props.nextLevel : 'expert';
		const currentVisitedLevel = (this.props.currentVisitedLevel === undefined || this.props.currentVisitedLevel === "") ? nextLevel : this.props.currentVisitedLevel;
		const levelToLoad = (nextLevel !== currentVisitedLevel) ? currentVisitedLevel : nextLevel;

		this.props.dispatch(actions.requestEmbryosTrainingQuestion(levelToLoad));
		this.questionRequested = true;

	}

	componentDidUpdate(prevProps) {

		if (!this.questionRequested) {

			const nextLevel = this.props.nextLevel !== null ? this.props.nextLevel : 'expert';
			const currentVisitedLevel = (this.props.currentVisitedLevel === undefined || this.props.currentVisitedLevel === "")  ? nextLevel : this.props.currentVisitedLevel;
			const levelToLoad = (nextLevel !== currentVisitedLevel) ? currentVisitedLevel : nextLevel;

			this.props.dispatch(actions.requestEmbryosTrainingQuestion(levelToLoad));
			this.questionRequested = true;

		}

		if (this.isTestAvailable(prevProps) && !this.isTestAvailable()) {
			this.setState({ animationHoverSeen: false });
		}

	}

	componentWillUnmount() {
		this.props.dispatch(actions.clearEmbryosTrainingQuestion());
	}

	getTitleByLevel(level) {

		let result = "";

		if(level === "beginner") {
			result = "Stage of an embryo";
		} else if(level === "intermediate") {
			result = "Quality of an embryo";
		} else if(level === "expert") {
			result = "Event timing of an embryo";
		}

		return result;

	}

	getSubtitleByLevel(level) {

		let result = "";

		if(level === "beginner") {
			result = "Assess correct development stage of an embryo";
		} else if(level === "intermediate") {
			result = "Assess correct quality of an embryo";
		} else if(level === "expert") {
			result = "Assess correct event timing of an embryo";
		}

		return result;

	}

	isTestAvailable(props = this.props) {
		return props.currentTrainedEmbryos >= 15 && props.accuracyPercentage >= 60;
	}

	moveReferenceLine = (value) => {
		this.props.dispatch(actions.uiMoveEmbryoTrainingReferenceLine(value));
	};

	moveOptionReferenceLine = (value) => {
		this.props.dispatch(actions.uiMoveEmbryoTrainingOptionReferenceLine(value));
	};

	removeBouncingAnimation = () => {
		if (this.isTestAvailable()) {
			this.setState({ animationHoverSeen: true });
		}
	};

	render() {

		const nextLevel = this.props.nextLevel !== null ? this.props.nextLevel : 'expert';
		const currentVisitedLevel = (this.props.currentVisitedLevel === undefined || this.props.currentVisitedLevel === "")  ? nextLevel : this.props.currentVisitedLevel;
		const levelToLoad = (nextLevel !== currentVisitedLevel) ? currentVisitedLevel : nextLevel;

		const currentTrainedEmbryos = this.props.currentTrainedEmbryos;
		const accuracyPercentage = (this.props.accuracyPercentage !== null) ? this.props.accuracyPercentage : 0;
		let accuracyPercentageClasses = "l-h " + (this.isTestAvailable() && !this.state.animationHoverSeen ? 'animation-jumping hightlight' : 'tooltip-top');

		const currentTrainingQuestion = this.props.currentTrainingQuestion;
		const currentTrainingQuestionSelectedOption = this.props.currentTrainingQuestionSelectedOption;

		let embryo = null;
		let embryoOptions = null;
		let embryoShowcase = null;

		if(Object.keys(currentTrainingQuestion).length > 0) {

			embryo = currentTrainingQuestion.embryo;
			embryoOptions = currentTrainingQuestion.options;

		}


		if (currentTrainingQuestionSelectedOption !== undefined && Object.keys(currentTrainingQuestionSelectedOption).length > 0) {
			const embryoOptionReferenceLine = this.props.currentTrainingEmbryoOptionReferenceLine;
			embryoShowcase = (
				<EmbryoShowcase
					option={currentTrainingQuestionSelectedOption}
					embryoReferenceLine={embryoOptionReferenceLine}
					onReferenceLineChange={this.moveOptionReferenceLine}
				/>
			);
		}

		const currentTrainingEmbryoReferenceLine = this.props.currentTrainingEmbryoReferenceLine;

		// No Data

		if(Object.keys(currentTrainingQuestion).length === 0) {

			return <div className="container-fluid"><Loading /></div>;

		}

		// Server Error

		else if (Object.keys(currentTrainingQuestion).length > 0 && currentTrainingQuestion.error) {

			return (

				<div className="error-container">
					Error: {currentTrainingQuestion.error}
				</div>

			);

		}

		// Training Question

		else if (Object.keys(currentTrainingQuestion).length > 0) {

			return (

				<div className="container embryo-stage-selection-container">

					<div className="row header-box">

						<div className="col-md-3 text-left">
							<NavLink to="/training" className="close-training-button l-h"><ArrowRight direction="left"/>Close training</NavLink>
						</div>
						<div className="col-md-6">
							<h2 className="embryo-stage-selection-title">{this.getTitleByLevel(levelToLoad)}</h2>
							<h3 className="embryo-stage-selection-subtitle">{this.getSubtitleByLevel(levelToLoad)}</h3>
						</div>
						<div className="col-md-3 text-right">
							<div className="right-status-wrapper">
								<span className="l-h tooltip-top tooltip-top__large" data-tooltip-large data-tooltip="Number of embryos you have trained on"><EmbryoTrainingIcon />{currentTrainedEmbryos}</span>
								<span onMouseOver={this.removeBouncingAnimation} className={accuracyPercentageClasses} data-tooltip={`${this.isTestAvailable() ? "Well done, you can start the test now." : "Success rate"} `}><GraphUpArrow />{accuracyPercentage}%</span>
							</div>
						</div>

					</div>

					<div className="row training-question-box">

						<div className="col-12">

							<div className="graph-box">

								<h3>Training embryo</h3>

								<EmbryoDetail
									embryo={embryo}
									current={currentTrainingEmbryoReferenceLine}
									onReferenceLineChange={this.moveReferenceLine}
								/>

							</div>

						</div>

					</div>

					<TrainingQuestionOptions

						type="training"

						nextLevel={levelToLoad}
						currentTrainingQuestion={currentTrainingQuestion}

						options={embryoOptions}

					/>

					{embryoShowcase}

				</div>

			);


		}

		return null;

	}


}

const mapStateToProps = (state) => {

	return {

		currentLevel: state.embryosTrainingReducer.data.currentLevel,
		nextLevel: state.embryosTrainingReducer.data.nextLevel,
		currentVisitedLevel: state.embryosTrainingReducer.currentVisitedLevel,

		currentTrainedEmbryos: state.embryosTrainingReducer.data.currentTrainedEmbryos,
		accuracyPercentage: state.embryosTrainingReducer.data.accuracyPercentage,

		currentTrainingQuestion: state.embryosTrainingReducer.currentTrainingQuestion,
		currentTrainingQuestionSelectedOption: state.embryosTrainingReducer.currentTrainingQuestionSelectedOption,
		currentTrainingEmbryoReferenceLine: state.embryosTrainingReducer.currentTrainingEmbryoReferenceLine,

		currentTrainingEmbryoOptionReferenceLine: state.embryosTrainingReducer.currentTrainingEmbryoOptionReferenceLine

	};

}

EmbryoStageSelection.propTypes = {

	currentLevel: PropTypes.string,
	nextLevel: PropTypes.string,
	currentVisitedLevel: PropTypes.string,

	currentTrainingQuestion: PropTypes.object,
	currentTrainingQuestionSelectedOption: PropTypes.object,
	currentTrainingEmbryoReferenceLine: PropTypes.number,

	currentTrainingEmbryoOptionReferenceLine: PropTypes.number,

	dispatch: PropTypes.func.isRequired

};

export default withRouter(connect(mapStateToProps)(EmbryoStageSelection));
