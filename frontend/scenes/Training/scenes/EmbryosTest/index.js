import React, { Component } from 'react';

import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';


import { goToTop } from 'react-scrollable-anchor';

import EmbryoDetail from '../../../../components/EmbryoDetail';
import TrainingQuestionOptions from '../../components/TrainingQuestionOptions';
import TrainingTestResult from '../../components/TrainingTestResult';
import ArrowRight from '../../../../components/ArrowRight';

import Loading from '../../../../components/Loading';

import { actions } from '../../../../reducers/embryos_training_reducer';

import './styles.scss';

class EmbryosTest extends Component {

	moveReferenceLineCache = [];

	componentDidMount() {

		this.props.dispatch(actions.requestEmbryosTrainingTest());

	}

	componentDidUpdate() {

		const levelToLoad = this.props.nextLevel;

		if (levelToLoad === undefined) {
			this.props.dispatch(actions.requestEmbryosTrainingUserProgress(undefined));
		}

	}

	onCloseTestClick = () => {
		this.props.dispatch(actions.uiCloseTest());
	};

	onConfirmSelectionClick = () => {

		const currentTrainingTestResult = this.props.currentTrainingTestResult;

		// Go to the Training Dashboard after answering the test

		if(Object.keys(currentTrainingTestResult).length > 0) {

			this.onCloseTestClick();
			this.props.history.push('/training');

		}

		// Submit Selection to get result

		else {

			const id = this.props.currentTrainingTest.testId;

			const answers = this.getAnswers();

			this.props.dispatch(actions.requestEmbryosTrainingTestResult(id, answers));
			goToTop();

		}

	};

	getTitleByLevel(level) {

		let result = "";

		if(level === "beginner") {
			result = "Test - Stages of development";
		} else if(level === "intermediate") {
			result = "Test - Quality of an embryo";
		} else if(level === "expert") {
			result = "Test - Event timing";
		}

		return result;

	}

	getSubtitleByLevel(level) {

		let result = "";

		if(level === "beginner") {
			result = "Select correct stage of development for every embryo below. You can have one incorrect answer.";
		} else if(level === "intermediate") {
			result = "Assess the quality of an embryo. The stage of development is given. You can have one incorrect answer.";
		} else if(level === "expert") {
			result = "Stage and quality being given, choose correct answer with respect to timing of the events of the cluster. You can have one incorrect answer.";
		}

		return result;

	}

	getAnswers() {

		let result = [];
		const selectedOptions = this.props.currentTrainingTestSelectedOptions;

		selectedOptions.forEach((option, index) => {

			let answer = {

				"questionId": option.questionId,
				"answer": option.value

			};

			result.push(answer);

		});

		return result;

	}

	isAllAnswersSelected() {

		let count = 0;
		const selectedOptions = this.props.currentTrainingTestSelectedOptions;

		selectedOptions.forEach((option, index) => {

			if(option !== "")
				++count;

		});

		return  (count === selectedOptions.length) ? true : false;

	}

	moveReferenceLine(index, value) {
		this.props.dispatch(actions.uiMoveEmbryoTestReferenceLine(index, value));
	}

	render() {

		const levelToLoad = this.props.nextLevel;

		const currentTrainingTest = this.props.currentTrainingTest;
		const currentTestEmbryoReferenceLines = this.props.currentTestEmbryoReferenceLines;

		let questions = null;
		let currentTrainingTestResult = this.props.currentTrainingTestResult;

		const isAllAnswersSelected = this.isAllAnswersSelected();
		let submitTestButtonClass = (isAllAnswersSelected) ? "btn btn-orange continue" : "btn btn-orange continue disabled";

		const currentTrainingTestLength = (currentTrainingTest !== undefined) ? Object.keys(currentTrainingTest).length : 0;
		let submitTestButtonText = "Submit selection";


		let questionsLength = 0;

		// Training Test Questions exist

		if (currentTrainingTestLength > 0) {

			questions = currentTrainingTest.questions;
			questionsLength = (questions !== undefined) ? questions.length : 0;

		}


		// Training Test Result exist

		let isSuccess = false;
		let testContainerClass = "embryos-test-container";
		let answeredQuestions = null;

		if(Object.keys(currentTrainingTestResult).length > 0) {

			submitTestButtonText = "Training dashboard";
			isSuccess = currentTrainingTestResult.success;
			testContainerClass = testContainerClass + (isSuccess ? " success" : " fail");

			answeredQuestions = currentTrainingTestResult.answeredQuestions;

		}

		const headerLayout = (Object.keys(currentTrainingTestResult).length === 0) ? <div className="col-md-6">
							<h2 className="embryo-stage-selection-title">{this.getTitleByLevel(levelToLoad)}</h2>
							<h3 className="embryo-stage-selection-subtitle">{this.getSubtitleByLevel(levelToLoad)}</h3>
							</div> : null;

		let closeTrainingButtonClass = (Object.keys(currentTrainingTestResult).length === 0) ? "col-md-3 text-left" : "col-md-2 text-left";

		return (currentTrainingTestLength > 0 && questions !== undefined) ? (

			<div className={testContainerClass}>

				<div className="container embryo-stage-selection-container">

					<div className="row">
						<div className={closeTrainingButtonClass}>
							<NavLink to="/training" className="close-training-button l-h" onClick={this.onCloseTestClick}><ArrowRight direction="left"/>Close test</NavLink>
						</div>

						{headerLayout}

						<TrainingTestResult
							level={currentTrainingTestResult.level}
							correctAnswers={currentTrainingTestResult.correctAnswers}
							accuracyPercentage={currentTrainingTestResult.accuracyPercentage}
							isSuccess={isSuccess}
							totalQuestions={questionsLength}
						/>

					</div>

					{

						questions.map((question, index) => {

							const embryo = question.embryo;
							const embryoOptions = question.options;

							const count = index + 1;

							let graphBoxClass = "graph-box";

							if(Object.keys(currentTrainingTestResult).length > 0) {

								graphBoxClass = graphBoxClass + ((answeredQuestions[index].success) ? " success" : " fail");

							}

							if (!this.moveReferenceLineCache[index]) {
								this.moveReferenceLineCache[index] = (value) => {
									this.moveReferenceLine(index, value);
								}
							}

							return (

									<div key={question.questionId} className="row training-question-box">

										<div className="col-12">

											<div className={graphBoxClass}>

												<h3>Embryo {count}/{questionsLength}</h3>
												{/* eslint-disable jsx-a11y/heading-has-content */}
												<h3 className="result" />
												{/* eslint-enable jsx-a11y/heading-has-content */}

												<EmbryoDetail
													embryo={embryo}
													current={currentTestEmbryoReferenceLines[index]}
													onReferenceLineChange={this.moveReferenceLineCache[index]}
												/>

												<TrainingQuestionOptions

													type="test"
													nextLevel={levelToLoad}

													options={embryoOptions}

													testQuestionId={question.questionId}
													testQuestionIndex={index}

												/>

											</div>

										</div>

									</div>

							);

						})

					}

					<a className={submitTestButtonClass} onClick={this.onConfirmSelectionClick}>{submitTestButtonText}<ArrowRight direction="right"/></a>

				</div>

			</div>

		) : <div className="container-fluid"><Loading /></div>;

	}

}

const mapStateToProps = (state) => {

	return {

		currentLevel: state.embryosTrainingReducer.data.currentLevel,
		nextLevel: state.embryosTrainingReducer.data.nextLevel,

		currentTrainingTest: state.embryosTrainingReducer.currentTrainingTest,
		currentTrainingTestSelectedOptions: state.embryosTrainingReducer.currentTrainingTestSelectedOptions,
		currentTestEmbryoReferenceLines: state.embryosTrainingReducer.currentTestEmbryoReferenceLines,

		currentTrainingTestResult: state.embryosTrainingReducer.currentTrainingTestResult

	};

};

EmbryosTest.propTypes = {

	currentLevel: PropTypes.string,
	nextLevel: PropTypes.string,

	currentTrainingTest: PropTypes.object,
	currentTrainingTestSelectedOptions: PropTypes.array,
	currentTestEmbryoReferenceLines: PropTypes.array,

	currentTrainingTestResult: PropTypes.object,

	dispatch: PropTypes.func.isRequired

};

export default withRouter(connect(mapStateToProps)(EmbryosTest));
