import React, { Component } from 'react';
import PropTypes from 'prop-types';

import EmbryoDetail from '../../../../components/EmbryoDetail';
import './styles.scss';

class EmbryoShowcase extends Component {

	render() {

		const embryoReferenceLine = this.props.embryoReferenceLine;

		const option = this.props.option;
		const embryo = option.embryo;
		const stage = option.value;

		return (embryo !== undefined && Object.keys(embryo).length > 0) ? (

			<div className="embryo-showcase-container">

				<div className="designation-chosen">
					<div className="designation-num">
						<span className="active-color">{stage}</span>
					</div>
				</div>

				<h2 className="embryo-showcase-title">{option.name}</h2>
				<h3 className="embryo-showcase-subtitle">{option.shortName}</h3>

				<p className="embryo-showcase-description">{option.description}</p>

				<div className="graph-box graph-box__transparent">
					<EmbryoDetail
						embryo={embryo}
						current={embryoReferenceLine}
						onReferenceLineChange={this.props.onReferenceLineChange}
					/>
				</div>

			</div>

		): null;

	}

}

EmbryoShowcase.propTypes = {
	option: PropTypes.object.isRequired,
	embryoReferenceLine: PropTypes.number.isRequired,
	onReferenceLineChange: PropTypes.func.isRequired,
};

export default EmbryoShowcase;
