import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TrophySuccess from '../../../../components/TrophySuccess';
import TrophyFail from '../../../../components/TrophyFail';

class TrainingTestResult extends Component {

	render() {

		const level = this.props.level;
		const correctAnswers = this.props.correctAnswers;
		const accuracyPercentage = this.props.accuracyPercentage;
		const isSuccess = this.props.isSuccess;
		const totalQuestions = this.props.totalQuestions;

		if(level !== undefined) {

			if(isSuccess) {

				return (

					<div className="col-md-8">
						<div className="success-header-wrapper">
							<TrophySuccess />
							<h2 className="embryo-stage-selection-title">Congratulations!</h2>
							<h3 className="embryo-stage-selection-undertitle">
								You achieved <span>{accuracyPercentage}% accuracy. Your level is now:</span> {level}
							</h3>
							<h3 className="embryo-stage-selection-subtitle">
								You assigned correctly {correctAnswers} of {totalQuestions} embryos.
							</h3>
						</div>
					</div>

				);

			} else {

				return (

					<div className="col-md-8">
						<div className="fail-header-wrapper">
							<TrophyFail />
							<h3 className="embryo-stage-selection-undertitle">
								You achieved <span>{accuracyPercentage}% accuracy. Your did not pass the test</span>
							</h3>
							<h3 className="embryo-stage-selection-subtitle">
								You assigned correctly {correctAnswers} of {totalQuestions} embryos.<br />
								Do not worry, you can try again.
							</h3>
						</div>
					</div>

				);

			}

		} else {

			return null;

		}

	}

}


TrainingTestResult.propTypes = {

	level: PropTypes.string,
	correctAnswers: PropTypes.number,
	accuracyPercentage: PropTypes.number,
	isSuccess: PropTypes.bool,
	totalQuestions: PropTypes.number

};

export default TrainingTestResult;
