import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './styles.scss';
import DoneCircle from '../../../../components/DoneCircle';

import { actions } from '../../../../reducers/embryos_training_reducer';

class TrainingPath extends Component {

	getLevelClass(selectedLevel) {

		const nextLevel = this.props.nextLevel !== null ? this.props.nextLevel : "expert";
		const currentVisitedLevel = this.props.currentVisitedLevel === '' ? nextLevel : this.props.currentVisitedLevel;

		return currentVisitedLevel === selectedLevel ? 'col active-column' : 'col';

	}

	getDoneCircle(level) {

		let show = false;
		const currentLevel = this.props.currentLevel;

		if(currentLevel === "beginner") {

			if(level === "beginner")
				show = true;

		} else if(currentLevel === "intermediate") {

			if(level === "beginner")
				show = true;
			else if(level === "intermediate")
				show = true;

		} else if(currentLevel === "expert") {
				show = true;
		}

		return (show ? <DoneCircle /> : null);


	}


	onLevelClick(selectedLevel) {

		const currentLevel = this.props.currentLevel;
		let levelToShow = "";

		// It's just a New User cannot change level
		if(currentLevel === "newUser") {

			return;

		} else {

			// Current Level: Beginner
			if(currentLevel === "beginner") {

				if(selectedLevel === "beginner") {
					levelToShow = "beginner";
				}

				else if(selectedLevel === "intermediate") {
					levelToShow = "intermediate";
				} else {
					return;
				}

			}

			// Current Level: Intermediate
			else if(currentLevel === "intermediate") {

				if(selectedLevel === "beginner") {
					levelToShow = "beginner";
				}
				else if(selectedLevel === "intermediate") {
					levelToShow = "intermediate";
				} else if(selectedLevel === "expert") {
					levelToShow = "expert";
				}

			}

			// Current Level: Expert
			else if(currentLevel === "expert") {

				if(selectedLevel === "beginner") {
					levelToShow = "beginner";
				}

				else if(selectedLevel === "intermediate") {
					levelToShow = "intermediate";
				}

				else if(selectedLevel === "expert") {
					levelToShow = "expert";
				}

			}

			this.props.dispatch(actions.requestEmbryosTrainingSelectLevel(levelToShow));

		}


	}

	componentDidMount() {

		if (this.props.animateFirstLevelUp) {
			setTimeout(() => this.props.dispatch(actions.uiAnimationOfFirstLevelupDone()), 3000);
		}
		if (this.props.animateSecondLevelUp) {
			setTimeout(() => this.props.dispatch(actions.uiAnimationOfSecondLevelupDone()), 3000);
		}

	}

	componentDidUpdate(prevProps) {

		if (this.props.animateFirstLevelUp && !prevProps.animateFirstLevelUp) {
			setTimeout(() => this.props.dispatch(actions.uiAnimationOfFirstLevelupDone()), 3000);
		}
		if (this.props.animateSecondLevelUp && !prevProps.animateSecondLevelUp) {
			setTimeout(() => this.props.dispatch(actions.uiAnimationOfSecondLevelupDone()), 3000);
		}

	}

	render() {

		const currentLevel = this.props.currentLevel;
		const levelsWrapperClass = "levels-wrapper " + currentLevel;

		const firstLevelClass = this.props.animateFirstLevelUp ?
			'animating' :
			(this.props.firstLevelUpAnimationSeen ? 'seen' : null);

		const secondLevelClass = this.props.animateSecondLevelUp ?
			'animating' :
			(this.props.secondLevelUpAnimationSeen ? 'seen' : null);

		return (
			<div className="row training-path-container">
				<div className="col">
					<div className="container">

						<h1 className="path">Your training path</h1>

						<div className={levelsWrapperClass}>
							<div className="row">
								<div className={this.getLevelClass("beginner")} onClick={this.onLevelClick.bind(this, "beginner")}>
									{/* Beginner */}
									<div className="level-container">
										<h2>
											<span className={firstLevelClass}>
												Beginner {this.getDoneCircle("beginner")}
											</span>
										</h2>
										<p>Stages of development training</p>
									</div>
								</div>

								<div className={this.getLevelClass("intermediate")} onClick={this.onLevelClick.bind(this, "intermediate")}>
									{/* Intermediate */}
									<div className="level-container">
										<h2>
											<span className={secondLevelClass}>
												Intermediate {this.getDoneCircle("intermediate")}
											</span>
										</h2>
										<p>Embryo quality training</p>

									</div>
								</div>

								<div className={this.getLevelClass("expert")} onClick={this.onLevelClick.bind(this, "expert")}>
									{/* Expert */}
									<div className="level-container last">
										<h2><span>Expert {this.getDoneCircle("expert")}</span></h2>
										<p>Clusters recognition training</p>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>


		);

	}

}

TrainingPath.propTypes = {

	currentLevel: PropTypes.string,
	nextLevel: PropTypes.string,
	currentVisitedLevel: PropTypes.string,

	currentTrainedEmbryos: PropTypes.number,
	totalEmbryos: PropTypes.number,

	dispatch: PropTypes.func.isRequired,

	animateFirstLevelUp: PropTypes.bool,
	firstLevelUpAnimationSeen: PropTypes.bool,
	animateSecondLevelUp: PropTypes.bool,
	secondLevelUpAnimationSeen: PropTypes.bool,

}

export default connect()(TrainingPath);
