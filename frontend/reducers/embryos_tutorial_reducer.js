import * as actionTypes from '../actions/tutorialActionTypes';


const initialState = {
	selectedReferenceLine: 0,
};


export default function tutorialReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.MOVE_REFERENCE_LINE:
			return {
				...state,
				selectedReferenceLine: action.payload,
			};
		default:
			return state;
	}
};
