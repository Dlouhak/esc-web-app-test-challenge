export const types = {
	EMBRYOS_LIBRARY_REQUESTED: 'EMBRYOS_LIBRARY_REQUESTED',
	EMBRYOS_LIBRARY_SUCCEEDED: 'EMBRYOS_LIBRARY_SUCCEEDED',
	EMBRYOS_LIBRARY_FAILED: 'EMBRYOS_LIBRARY_FAILED',

	EMBRYOS_LOAD_MORE_REQUESTED: 'EMBRYOS_LOAD_MORE_REQUESTED',
	EMBRYOS_LOAD_MORE_SUCCEEDED: 'EMBRYOS_LOAD_MORE_SUCCEEDED',
	EMBRYOS_LOAD_MORE_FAILED: 'EMBRYOS_LOAD_MORE_FAILED',

	EMBRYOS_LIST_VIEW_TOGGLE: 'EMBRYOS_LIST_VIEW_TOGGLE',

	EMBRYOS_COMPARE_PLAY: 'EMBRYOS_COMPARE_PLAY',
	EMBRYOS_COMPARE_STOP: 'EMBRYOS_COMPARE_STOP',
	EMBRYOS_COMPARE_MOVE_REFERENCE_LINE: 'EMBRYOS_COMPARE_MOVE_REFERENCE_LINE',

	EMBRYOS_COMPARE_SHOW: 'EMBRYOS_COMPARE_SHOW',
	EMBRYOS_COMPARE_COLLAPSE: 'EMBRYOS_COMPARE_COLLAPSE',

	EMBRYOS_ADD_TO_COMPARE_FIRST_EMBRYO: 'EMBRYOS_ADD_TO_COMPARE_FIRST_EMBRYO',
	EMBRYOS_ADD_TO_COMPARE_SECOND_EMBRYO: 'EMBRYOS_ADD_TO_COMPARE_SECOND_EMBRYO',

	EMBRYOS_REMOVE_FROM_COMPARE_FIRST_EMBRYO: 'EMBRYOS_REMOVE_FROM_COMPARE_FIRST_EMBRYO',
	EMBRYOS_REMOVE_FROM_COMPARE_SECOND_EMBRYO: 'EMBRYOS_REMOVE_FROM_COMPARE_SECOND_EMBRYO',

	EMBRYOS_FILTER_NODE_SELECTED: 'EMBRYOS_FILTER_NODE_SELECTED',
	EMBRYOS_FILTER_NODE_UNSELECTED: 'EMBRYOS_FILTER_NODE_UNSELECTED',
	EMBRYOS_FILTER_ENTRY_SELECTED: 'EMBRYOS_FILTER_ENTRY_SELECTED',
	EMBRYOS_FILTER_ENTRY_UNSELECTED: 'EMBRYOS_FILTER_ENTRY_UNSELECTED',
	EMBRYOS_FILTER_RESET: 'EMBRYOS_FILTER_RESET',

	EMBRYOS_SELECT: 'EMBRYOS_SELECT',
	EMBRYOS_UNSELECT: 'EMBRYOS_UNSELECT',
	EMBRYOS_DETAILS_MOVE_REFERENCE_LINE: 'EMBRYOS_DETAILS_MOVE_REFERENCE_LINE',
};


export const initialState = {
	data: {
		embryosByCategories: {},
	},

	isLoading: false,
	error: null,

	listView: false,

	currentComparePlay: false,
	currentComparePlayPosition: 0,

	isCompareCompleteVisible: false,

	currentCompareFirstEmbryo: {},
	currentCompareFirstCategory: {},

	currentCompareSecondEmbryo: {},
	currentCompareSecondCategory: {},

	currentFilteredEmbryos: null,

	currentSelectedEmbryo: {},
	currentSelectedEmbryoReferenceLine: 0,
};


const getEmbryoCluster = (embryo, embryosByCategories) => {
	const category = embryosByCategories[embryo.phase_of_development] || [];
	const length = category.length;

	for (let i = 0; i < length; i++) {
		if (category[i].cluster === embryo.cluster) {
			const { values, ...other } = category[i];
			return other;
		}
	}

	return null;
};


export default function embryosLibraryReducer(state = initialState, action) {
	switch (action.type) {
		// Server Actions

		case types.EMBRYOS_LIBRARY_REQUESTED: {
			return {
				...state,
				isLoading: true,
			};
		}

		case types.EMBRYOS_LIBRARY_SUCCEEDED: {
			return {
				...state,
				isLoading: false,
				data: action.payload,
			};
		}

		case types.EMBRYOS_LIBRARY_FAILED: {
			return {
				...state,
				isLoading: false,
				error: action.error,
			};
		}

		case types.EMBRYOS_LOAD_MORE_REQUESTED: {
			return {
				...state,
				isLoading: true,
			};
		}

		case types.EMBRYOS_LOAD_MORE_SUCCEEDED: {
			const clusters = state.data.embryosByCategories[action.category];
			const newClusters = clusters.map((cluster) => {
				if (cluster.cluster === action.cluster) {
					return {
						...cluster,
						values: cluster.values.concat(action.payload.values),
						lastValuesCounter: action.payload.values.length,
					};
				}
				return cluster;
			});

			return {
				...state,
				data: {
					...state.data,
					embryosByCategories: {
						...state.data.embryosByCategories,
						[action.category]: newClusters,
					},
				},
			};
		}

		case types.EMBRYOS_LOAD_MORE_FAILED: {
			return {
				...state,
				isLoading: false,
				error: action.error,
			};
		}

		// UI Actions

		case types.EMBRYOS_LIST_VIEW_TOGGLE: {
			return {
				...state,
				listView: action.payload,
			};
		}

		case types.EMBRYOS_COMPARE_PLAY: {
			return {
				...state,
				currentComparePlay: true,
				currentComparePlayPosition: action.payload,
			};
		}

		case types.EMBRYOS_COMPARE_STOP: {
			return {
				...state,
				currentComparePlay: false,
				currentComparePlayPosition: action.payload,
			};
		}

		case types.EMBRYOS_COMPARE_MOVE_REFERENCE_LINE: {
			return {
				...state,
				currentComparePlayPosition: action.payload,
			};
		}

		case types.EMBRYOS_COMPARE_SHOW: {
			return {
				...state,
				isCompareCompleteVisible: true,
			};
		}

		case types.EMBRYOS_COMPARE_COLLAPSE: {
			return {
				...state,
				isCompareCompleteVisible: false,
			};
		}

		case types.EMBRYOS_ADD_TO_COMPARE_FIRST_EMBRYO: {
			return {
				...state,
				currentCompareFirstEmbryo: action.embryo,
				currentCompareFirstCategory: action.cluster || getEmbryoCluster(action.embryo, state.data.embryosByCategories),
				isCompareCompleteVisible: true,
			};
		}

		case types.EMBRYOS_ADD_TO_COMPARE_SECOND_EMBRYO: {
			return {
				...state,
				currentCompareSecondEmbryo: action.embryo,
				currentCompareSecondCategory: action.cluster || getEmbryoCluster(action.embryo, state.data.embryosByCategories),
				isCompareCompleteVisible: true,
			};
		}

		case types.EMBRYOS_REMOVE_FROM_COMPARE_FIRST_EMBRYO: {
			return {
				...state,
				currentCompareFirstEmbryo: {},
				currentCompareFirstCategory: {},
				isCompareCompleteVisible: true,
			};
		}

		case types.EMBRYOS_REMOVE_FROM_COMPARE_SECOND_EMBRYO: {
			return {
				...state,
				currentCompareSecondEmbryo: {},
				currentCompareSecondCategory: {},
				isCompareCompleteVisible: true,
			};
		}

		case types.EMBRYOS_FILTER_NODE_SELECTED: {
			return {
				...state,
				currentFilteredEmbryos: {
					...state.currentFilteredEmbryos,
					[action.category]: action.clusters,
				}
			};
		}

		case types.EMBRYOS_FILTER_NODE_UNSELECTED: {
			// Remove Unselected Node
			const { [action.category]: _, ...newFilteredEmbryos } = state.currentFilteredEmbryos || {};

			return {
				...state,
				currentFilteredEmbryos: Object.keys(newFilteredEmbryos).length > 0 ? newFilteredEmbryos : null,
			};
		}

		case types.EMBRYOS_FILTER_ENTRY_SELECTED: {
			const currentCategory = (state.currentFilteredEmbryos || {})[action.category] || [];
			return {
				...state,
				currentFilteredEmbryos: {
					...state.currentFilteredEmbryos,
					[action.category]: [
						...currentCategory,
						action.cluster,
					],
				},
			};
		}

		case types.EMBRYOS_FILTER_ENTRY_UNSELECTED: {
			const { [action.category]: currentCategory, ...otherCategories } = state.currentFilteredEmbryos || {};

			// Remove Unselected Cluster from Category
			const updatedCategory = (currentCategory || []).filter(cluster => cluster !== action.cluster);

			// If the Updated Category Size = 0, we need to remove the category from the State
			// If the Updated Category Size > 0, we can keep it in the State
			const newFilteredEmbryos = {
				...otherCategories,
				...(updatedCategory.length > 0 ? { [action.category]: updatedCategory } : {}),
			};

			return {
				...state,
				currentFilteredEmbryos: Object.keys(newFilteredEmbryos).length > 0 ? newFilteredEmbryos : null,
			};
		}

		case types.EMBRYOS_FILTER_RESET: {
			return {
				...state,
				currentFilteredEmbryos: {
					[action.category]: [
						action.cluster,
					],
				},
			};
		}

		case types.EMBRYOS_SELECT: {
			return {
				...state,
				currentSelectedEmbryo: action.payload,
			};
		}

		case types.EMBRYOS_UNSELECT: {
			return {
				...state,
				currentSelectedEmbryo: {},
			};
		}

		case types.EMBRYOS_DETAILS_MOVE_REFERENCE_LINE: {
			return {
				...state,
				currentSelectedEmbryoReferenceLine: action.payload,
			};
		}

		default:
			return state;
	}
}

export const actions = {
	requestEmbryos: (limit, embryoId) => ({ type: types.EMBRYOS_LIBRARY_REQUESTED, limit, embryoId }),
	requestSucceededEmbryos : (data) => ({ type: types.EMBRYOS_LIBRARY_SUCCEEDED, payload: data }),
	requestFailedEmbryos : (errorMessage) => ({ type: types.EMBRYOS_LIBRARY_FAILED, error: errorMessage }),

	requestEmbryosLoadMore: (category, cluster, offset, limit) => ({ type: types.EMBRYOS_LOAD_MORE_REQUESTED, category, cluster, offset, limit }),
	requestSucceededEmbryosLoadMore: (category, cluster, offset, data) => ({ type: types.EMBRYOS_LOAD_MORE_SUCCEEDED, category, cluster, offset, payload: data }),
	requestFailedEmbryosLoadMore : (errorMessage) => ({ type: types.EMBRYOS_LOAD_MORE_FAILED, error: errorMessage }),

	uiChangeEmbryosListViewToggle : (value) => ({ type: types.EMBRYOS_LIST_VIEW_TOGGLE, payload: value }),

	uiPlayCompareChart: (position) => ({ type: types.EMBRYOS_COMPARE_PLAY, payload: position }),
	uiStopCompareChart: (position) => ({ type: types.EMBRYOS_COMPARE_STOP, payload: position }),
	uiMoveCurrentCompareEmbryoReferenceLine: (value) => ({ type: types.EMBRYOS_COMPARE_MOVE_REFERENCE_LINE, payload: value }),

	uiCompareShow: () => ({ type: types.EMBRYOS_COMPARE_SHOW }),
	uiCompareCollapse: () => ({ type: types.EMBRYOS_COMPARE_COLLAPSE }),

	uiAddToCompareFirstEmbryo: (embryo, cluster = null) => ({ type: types.EMBRYOS_ADD_TO_COMPARE_FIRST_EMBRYO, embryo, cluster }),
	uiAddToCompareSecondEmbryo: (embryo, cluster = null) => ({ type: types.EMBRYOS_ADD_TO_COMPARE_SECOND_EMBRYO, embryo, cluster }),

	uiRemoveFromCompareFirstEmbryo: () => ({ type: types.EMBRYOS_REMOVE_FROM_COMPARE_FIRST_EMBRYO }),
	uiRemoveFromCompareSecondEmbryo: () => ({ type: types.EMBRYOS_REMOVE_FROM_COMPARE_SECOND_EMBRYO }),

	uiSelectEmbryoNodeFilter: (category, clusters) => ({ type: types.EMBRYOS_FILTER_NODE_SELECTED, category, clusters }),
	uiUnselectEmbryoNodeFilter: (category) => ({ type: types.EMBRYOS_FILTER_NODE_UNSELECTED, category }),
	uiSelectEmbryoEntryFilter: (category, cluster) => ({ type: types.EMBRYOS_FILTER_ENTRY_SELECTED, category, cluster }),
	uiUnselectEmbryoEntryFilter: (category, cluster) => ({ type: types.EMBRYOS_FILTER_ENTRY_UNSELECTED, category, cluster }),
	uiResetEmbryoFilter: (category, cluster) => ({ type: types.EMBRYOS_FILTER_RESET, category, cluster }),

	uiUnselectEmbryo: () => ({ type: types.EMBRYOS_UNSELECT }),
	uiSelectEmbryo: (embryo) => ({ type: types.EMBRYOS_SELECT, payload: embryo }),
	uiMoveCurrentSelectedEmbryoReferenceLine: (value) => ({ type: types.EMBRYOS_DETAILS_MOVE_REFERENCE_LINE, payload: value }),
};
