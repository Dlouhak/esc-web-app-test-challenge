
export const types = {

	EMBRYOS_TRAINING_USER_PROGRESS_REQUESTED: 'EMBRYOS_TRAINING_USER_PROGRESS_REQUESTED',
	EMBRYOS_TRAINING_USER_PROGRESS_SUCCEEDED: 'EMBRYOS_TRAINING_USER_PROGRESS_SUCCEEDED',
	EMBRYOS_TRAINING_USER_PROGRESS_FAILED: 'EMBRYOS_TRAINING_USER_PROGRESS_FAILED',

	EMBRYOS_TRAINING_TRAINING_QUESTION_REQUESTED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_REQUESTED',
	EMBRYOS_TRAINING_TRAINING_QUESTION_SUCCEEDED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_SUCCEEDED',
	EMBRYOS_TRAINING_TRAINING_QUESTION_FAILED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_FAILED',

	EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_REQUESTED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_REQUESTED',
	EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_SUCCEEDED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_SUCCEEDED',
	EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_FAILED: 'EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_FAILED',

	EMBRYOS_TRAINING_TRAINING_QUESTION_SELECT_OPTION: 'EMBRYOS_TRAINING_TRAINING_QUESTION_SELECT_OPTION',

	EMBRYOS_TRAINING_TRAINING_QUESTION_MOVE_REFERENCE_LINE: 'EMBRYOS_TRAINING_TRAINING_QUESTION_MOVE_REFERENCE_LINE',
	EMBRYOS_TRAINING_TRAINING_QUESTION_OPTION_MOVE_REFERENCE_LINE: 'EMBRYOS_TRAINING_TRAINING_QUESTION_OPTION_MOVE_REFERENCE_LINE',

	EMBRYOS_TRAINING_TRAINING_TEST_REQUESTED: 'EMBRYOS_TRAINING_TRAINING_TEST_REQUESTED',
	EMBRYOS_TRAINING_TRAINING_TEST_SUCCEEDED: 'EMBRYOS_TRAINING_TRAINING_TEST_SUCCEEDED',
	EMBRYOS_TRAINING_TRAINING_TEST_FAILED: 'EMBRYOS_TRAINING_TRAINING_TEST_FAILED',

	EMBRYOS_TRAINING_TRAINING_TEST_RESULT_REQUESTED: 'EMBRYOS_TRAINING_TRAINING_TEST_RESULT_REQUESTED',
	EMBRYOS_TRAINING_TRAINING_TEST_RESULT_SUCCEEDED: 'EMBRYOS_TRAINING_TRAINING_TEST_RESULT_SUCCEEDED',
	EMBRYOS_TRAINING_TRAINING_TEST_RESULT_FAILED: 'EMBRYOS_TRAINING_TRAINING_TEST_RESULT_FAILED',

	EMBRYOS_TRAINING_TRAINING_TEST_SELECT_OPTION: 'EMBRYOS_TRAINING_TRAINING_TEST_SELECT_OPTION',

	EMBRYOS_TRAINING_TRAINING_TEST_MOVE_REFERENCE_LINE: 'EMBRYOS_TRAINING_TRAINING_TEST_MOVE_REFERENCE_LINE',

	EMBRYOS_TRAINING_CURRENT_VISITED_LEVEL: 'EMBRYOS_TRAINING_CURRENT_VISITED_LEVEL',

	EMBRYOS_TRAINING_CLOSE_TEST: 'EMBRYOS_TRAINING_CLOSE_TEST',

	EMBRYOS_TRAINING_SELECT_LEVEL_REQUESTED: 'EMBRYOS_TRAINING_SELECT_LEVEL_REQUESTED',
	EMBRYOS_TRAINING_SELECT_LEVEL_SUCCEEDED: 'EMBRYOS_TRAINING_SELECT_LEVEL_SUCCEEDED',
	EMBRYOS_TRAINING_SELECT_LEVEL_FAILED: 'EMBRYOS_TRAINING_SELECT_LEVEL_FAILED',

	ANIMATE_FIRST_LEVELUP_CALLBACK: 'ANIMATE_FIRST_LEVELUP_CALLBACK',
	ANIMATE_SECOND_LEVELUP_CALLBACK: 'ANIMATE_SECOND_LEVELUP_CALLBACK',

	CLEAR_EMBRYOS_TRAINING_QUESTION: 'CLEAR_EMBRYOS_TRAINING_QUESTION'

}


export const initialState = {

	data: {},

	currentVisitedLevel: "",

	currentTrainingQuestion: {},
	currentTrainingQuestionResult: {},

	currentTrainingQuestionSelectedOption: {},
	currentTrainingEmbryoReferenceLine: 0,
	currentTrainingEmbryoOptionReferenceLine: 0,

	currentTrainingTest: {},
	currentTrainingTestResult: {},

	currentTrainingTestSelectedOptions: [],
	currentTestEmbryoReferenceLines: [],

	isLoading: false,
	error: null,

	animateFirstLevelUp: false,
	firstLevelUpAnimationSeen: false,
	animateSecondLevelUp: false,
	secondLevelUpAnimationSeen: false,

}

export default function embryosTrainingReducer(state = initialState, action) {

	switch(action.type) {

		/* User Progress */

		case types.EMBRYOS_TRAINING_USER_PROGRESS_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true,
			})

		}

		case types.EMBRYOS_TRAINING_USER_PROGRESS_SUCCEEDED: {

			const level = action.payload.currentLevel;
			const secondLevelUpAnimationSeen = level === 'intermediate' || level === 'expert';
			const firstLevelUpAnimationSeen = secondLevelUpAnimationSeen || level === 'beginner';

			return Object.assign({}, state, {

				data: action.payload,
				isLoading: false,
				firstLevelUpAnimationSeen,
				secondLevelUpAnimationSeen,

			})

		}

		case types.EMBRYOS_TRAINING_USER_PROGRESS_FAILED: {

			return Object.assign({}, state, {
				isLoading: false,
				error: action.error,
			})

		}


		case types.EMBRYOS_TRAINING_SELECT_LEVEL_SUCCEEDED: {


			return Object.assign({}, state, {

				data: action.payload,
				currentVisitedLevel: action.selectedLevel,
				isLoading: false,

			})

		}

		case types.EMBRYOS_TRAINING_SELECT_LEVEL_FAILED: {

			return Object.assign({}, state, {
				isLoading: false,
				error: action.error,
			})

		}


		/* Training Question */

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true,
			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_SUCCEEDED: {

			return Object.assign({}, state, {

				currentTrainingQuestion: action.question,
				currentTrainingQuestionResult: {},
				currentTrainingQuestionSelectedOption: {},
				isLoading: false,

			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_FAILED: {

			return Object.assign({}, state, {
				isLoading: false,
				error: action.error,
			})

		}



		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true,
			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_SUCCEEDED: {

			return Object.assign({}, state, {

				currentTrainingQuestionResult: action.payload,
				data: {

					...state.data,
					accuracyPercentage: action.payload.accuracyPercentage,
					currentTrainedEmbryos: action.payload.currentTrainedEmbryos

				},
				isLoading: false,

			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_FAILED: {

			return Object.assign({}, state, {
				isLoading: false,
				error: action.error,
			})

		}



		/* Training Test */

		case types.EMBRYOS_TRAINING_TRAINING_TEST_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true,
			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_TEST_SUCCEEDED: {


			const questions = action.payload.questions;
			let referenceLines = [];
			let selectedOptions = [];

			questions.forEach(index => {

				referenceLines.push(0);
				selectedOptions.push("");

			});

			return Object.assign({}, state, {

				currentTrainingTest: action.payload,
				currentTestEmbryoReferenceLines: referenceLines,
				currentTrainingTestSelectedOptions: selectedOptions,
				isLoading: false,

			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_TEST_FAILED: {

			return Object.assign({}, state, {

				isLoading: false,
				error: action.error,

			})

		}


		case types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true,
			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_SUCCEEDED: {

			let temp_animateFirstLevelUp = state.animateFirstLevelUp;
			let temp_animateSecondLevelUp = state.animateSecondLevelUp;

			if (action.payload.success) {
				if (!state.firstLevelUpAnimationSeen) {
					temp_animateFirstLevelUp = true;
				} else if (!state.secondLevelUpAnimationSeen) {
					temp_animateSecondLevelUp = true;
				}
			}

			return Object.assign({}, state, {
				animateFirstLevelUp: temp_animateFirstLevelUp,
				animateSecondLevelUp: temp_animateSecondLevelUp,

				currentTrainingTestResult: action.payload,
				isLoading: false,

			});

		}

		case types.ANIMATE_FIRST_LEVELUP_CALLBACK: {

			return Object.assign({}, state, {
				animateFirstLevelUp: false,
				firstLevelUpAnimationSeen: true,

				isLoading: false,

			})

		}

		case types.ANIMATE_SECOND_LEVELUP_CALLBACK: {

			return Object.assign({}, state, {
				animateSecondLevelUp: false,
				secondLevelUpAnimationSeen: true, // for future expansion of test

				isLoading: false,

			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_FAILED: {

			return Object.assign({}, state, {

				isLoading: false,
				error: action.error,

			})

		}


		/* UI Actions */

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_SELECT_OPTION: {

			return Object.assign({}, state, {

				currentTrainingQuestionSelectedOption: action.payload,

			})

		}


		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_MOVE_REFERENCE_LINE: {

			return Object.assign({}, state, {

				currentTrainingEmbryoReferenceLine: action.payload,

			})

		}

		case types.EMBRYOS_TRAINING_TRAINING_QUESTION_OPTION_MOVE_REFERENCE_LINE: {

			return Object.assign({}, state, {

				currentTrainingEmbryoOptionReferenceLine: action.payload,

			})

		}


		case types.EMBRYOS_TRAINING_TRAINING_TEST_SELECT_OPTION: {

			action.payload["questionId"] = action.questionId;

			const newSelectedOptions = [...state.currentTrainingTestSelectedOptions];
			newSelectedOptions[action.index] = action.payload;

			return Object.assign({}, state, {

				currentTrainingTestSelectedOptions: newSelectedOptions,

			})


		}

		case types.EMBRYOS_TRAINING_TRAINING_TEST_MOVE_REFERENCE_LINE: {

			const index = action.index;
			const value = action.value;

			const referenceLines =[...state.currentTestEmbryoReferenceLines];
			referenceLines[index] = value;

			return Object.assign({}, state, {

				currentTestEmbryoReferenceLines: referenceLines,

			})

		}

		case types.EMBRYOS_TRAINING_CURRENT_VISITED_LEVEL: {

			return Object.assign({}, state, {

				currentVisitedLevel: action.payload,

			})

		}

		case types.EMBRYOS_TRAINING_CLOSE_TEST: {

			return Object.assign({}, state, {

				currentTrainingTest: {},
				currentTrainingTestResult: {},

				currentTrainingTestSelectedOptions: [],
				currentTestEmbryoReferenceLines: [],

			})

		}

		case types.CLEAR_EMBRYOS_TRAINING_QUESTION: {

			return Object.assign({}, state, {

				currentTrainingQuestion: {},
				currentTrainingQuestionResult: {},

			})

		}


		default:
			return state


	}

}

export const actions = {

	requestEmbryosTrainingUserProgress: (data) => ({ type: types.EMBRYOS_TRAINING_USER_PROGRESS_REQUESTED, payload: data }),
	requestSucceededEmbryosTrainingUserProgress : (data) => ({ type: types.EMBRYOS_TRAINING_USER_PROGRESS_SUCCEEDED, payload: data }),
	requestFailedEmbryosTrainingUserProgress : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_USER_PROGRESS_FAILED, error: errorMessage }),

	requestEmbryosTrainingSelectLevel: (data, selectedLevel) => ({ type: types.EMBRYOS_TRAINING_SELECT_LEVEL_REQUESTED, payload: data, selectedLevel }),
	requestSucceededEmbryosTrainingSelectLevel : (data, selectedLevel) => ({ type: types.EMBRYOS_TRAINING_SELECT_LEVEL_SUCCEEDED, payload: data, selectedLevel }),
	requestFailedEmbryosTrainingSelectLevel : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_SELECT_LEVEL_FAILED, error: errorMessage }),


	requestEmbryosTrainingQuestion: (data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_REQUESTED, payload: data }),
	requestSucceededEmbryosTrainingQuestion : (question) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_SUCCEEDED, question }),
	requestFailedEmbryosTrainingQuestion : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_FAILED, error: errorMessage }),

	requestEmbryosTrainingQuestionResult: (id, answer) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_REQUESTED, id, answer }),
	requestSucceededEmbryosTrainingQuestionResult : (data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_SUCCEEDED, payload: data }),
	requestFailedEmbryosTrainingQuestionResult : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_RESULT_FAILED, error: errorMessage }),

	requestEmbryosTrainingTest: () => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_REQUESTED }),
	requestSucceededEmbryosTrainingTest : (data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_SUCCEEDED, payload: data }),
	requestFailedEmbryosTrainingTest : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_FAILED, error: errorMessage }),

	requestEmbryosTrainingTestResult: (id, answers) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_REQUESTED, id, answers }),
	requestSucceededEmbryosTrainingTestResult : (data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_SUCCEEDED, payload: data }),
	requestFailedEmbryosTrainingTestResult : (errorMessage) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_RESULT_FAILED, error: errorMessage }),

	clearEmbryosTrainingQuestion: () => ({ type: types.CLEAR_EMBRYOS_TRAINING_QUESTION }),

	uiSelectOptionAtTrainingQuestion: (data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_SELECT_OPTION, payload: data}),
	uiMoveEmbryoTrainingReferenceLine: (value) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_MOVE_REFERENCE_LINE, payload: value }),
	uiMoveEmbryoTrainingOptionReferenceLine: (value) => ({ type: types.EMBRYOS_TRAINING_TRAINING_QUESTION_OPTION_MOVE_REFERENCE_LINE, payload: value }),

	uiSelectOptionAtTestQuestion: (questionId, index, data) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_SELECT_OPTION, questionId, index, payload: data}),
	uiMoveEmbryoTestReferenceLine: (index, value) => ({ type: types.EMBRYOS_TRAINING_TRAINING_TEST_MOVE_REFERENCE_LINE, index, value }),

	uiSelectVisitedLevel: (value) => ({ type: types.EMBRYOS_TRAINING_CURRENT_VISITED_LEVEL, payload: value }),

	uiCloseTest: () => ({ type: types.EMBRYOS_TRAINING_CLOSE_TEST }),

	uiAnimationOfFirstLevelupDone: () => ({ type: types.ANIMATE_FIRST_LEVELUP_CALLBACK }),
	uiAnimationOfSecondLevelupDone: () => ({ type: types.ANIMATE_SECOND_LEVELUP_CALLBACK }),

}
