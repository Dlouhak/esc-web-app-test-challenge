import React from 'react';
import './styles.scss';


export default function GraphIconTwo() {
	return (
		<div className="arrow-down-graph-wrapper">
			<svg width="20px" height="14px" viewBox="0 0 20 14">
				<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-546.000000, -862.000000)">
					<path d="M547.875083,875 C550.314495,868.014269 552.992916,866.014269 555.910346,869 C561.773091,875 560.790189,863 565.875083,863" id="icon-graph2" stroke="#7BCBF1" strokeWidth="2" />
				</g>
			</svg>
		</div>
	);
};
