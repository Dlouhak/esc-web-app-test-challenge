import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Learning from '../../scenes/Learning';
import EmbryoLibrary from '../../scenes/EmbryoLibrary';
import Training from '../../scenes/Training';

import Compare from '../../scenes/EmbryoLibrary/scenes/Compare';

class MainRouter extends Component {

	render() {

		return (

			<main role="application">

				<Switch>

					<Route
						exact
						path="/"
						component={EmbryoLibrary}
					/>

					<Route
						path="/learning"
						component={Learning}
					/>

					<Route
						path="/embryo_library"
						component={EmbryoLibrary}
					/>

					<Route
						path="/training"
						component={Training}
					/>

					<Route
						path="/embryos_compare/:idFirstEmbryo/:idSecondEmbryo"
						exact
						component={Compare}
					/>

					<Redirect
						from="/embryos_compare"
						to="/embryo_library"
					/>

				</Switch>

			</main>

		);

	}

}

export default MainRouter;
