import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import $ from 'jquery';

import {withRouter} from "react-router-dom";

import './styles.scss';
import logo from '../../img/icon-esclogo.svg';
import profilePicture from '../../img/avatar.png';
import PropTypes from 'prop-types';


class Header extends Component {


	constructor(props)	{
		super(props);
		this.state = { menuIsOpen: false };
	}

	toggleMenuClick() {

		$('.logo-drop').toggleClass('in');
		$('.fa-chevron-down').toggleClass('up');
		this.setState( { menuIsOpen : !this.state.menuIsOpen } );

	}


	render() {

		const pathname = this.props.location.pathname;
		const mainPageNavLinkClassName = (pathname === "/") ? "nav-link active" : "nav-link";

		return (

			<nav className="header-container navbar navbar-expand-lg navbar-light navbar-fixed-top">

				<NavLink to="/" className="navbar-brand">
					<img src={logo} alt="logo" width="60" height="33" />
					<span>ESC</span>
				</NavLink>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon" />
				</button>

				<div className="collapse navbar-collapse" id="navbarSupportedContent">

					<ul className="navbar-nav">

						<li className="nav-item">
							<NavLink to="/learning" className="nav-link" activeClassName="active">Learning</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/training" className="nav-link" activeClassName="active">Training</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/embryo_library" className={mainPageNavLinkClassName} activeClassName="active">Embryo Library</NavLink>
						</li>

					</ul>

					{this.props.cleanCacheButton ? (
						<a href="?do=cleanCache" className="btn btn-sm btn-default">
							Refresh Results
						</a>
					) : null}

					<div className="user-info">
						<div className="logo-drop-open">
							<span className="user-name">
								<a onClick={this.toggleMenuClick.bind(this)}>

									<img src={profilePicture} alt="logo" width="32" height="32" />
									<p>{this.props.userName}</p>
									<i className="fa fa-chevron-down rotate" aria-hidden="true" />

								</a>
							</span>

						</div>
						<div className="logo-drop">
							<ul>
								<li>
									<a href={`${process.env.PUBLIC_URL}/user`}>Settings</a>
								</li>
								<li>
									<a href={`${process.env.PUBLIC_URL}/logout`}>Log out</a>
								</li>
							</ul>
						</div>
					</div>

				</div>

			</nav>

		);

	}

}

Header.propTypes = {
	userName: PropTypes.string,
	cleanCacheButton: PropTypes.bool,
};

export default withRouter(Header);
