import React, { Component } from 'react';
import './styles.scss';

class ArrowDown extends Component {

	getArrowDownColor() {

		const collapseType = this.props.collapseType;

		if(collapseType === "mild") {
			return "#39A4CD";

		} else if(collapseType === "veryMild") {
			return "#61B945";

		} else if(collapseType === "extensive") {
			return "#D7D721";

		} else if(collapseType === "veryExtensive") {
			return "#E13E52";

		} else {
			return "#E13E52";

		}

	}

	render() {


			const finalCY = (this.props.cy <= 16) ? "0px" : this.props.cy - 24;
			const arrowColor = this.getArrowDownColor();

			return (

				<svg x={this.props.cx - 6.5} y={finalCY} width="13px" height="16px">
					<g id="Guidelines" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<g id="Icons" transform="translate(-676.000000, -474.000000)" fillRule="nonzero" fill={arrowColor}>
							<path d="M684.583628,483.604337 C684.330335,483.857923 684.125,483.773167 684.125,483.410954 L684.125,474.65254 C684.125,474.292152 683.836584,474 683.475125,474 L681.524875,474 C681.165959,474 680.875,474.290328 680.875,474.65254 L680.875,483.410954 C680.875,483.771342 680.668641,483.856898 680.416372,483.604337 L676.458628,479.64203 C676.205335,479.388444 676,479.474208 676,479.832395 L676,483.134845 C676,483.493566 676.205997,483.989141 676.46129,484.242919 L682.061807,489.810187 C682.31657,490.063438 682.727635,490.063965 682.983708,489.807597 L688.539389,484.245509 C688.793778,483.990827 689,483.493032 689,483.134845 L689,479.832395 C689,479.473674 688.793641,479.38947 688.541372,479.64203 L684.583628,483.604337 Z" id="icon-grapharrow-down" />
						</g>
					</g>
				</svg>

			);

	}

}

export default ArrowDown;
