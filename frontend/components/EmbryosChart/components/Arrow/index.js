import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ArrowDown from './components/ArrowDown';
import ArrowUp from './components/ArrowUp';


class Arrow extends Component {

	findChartEvent() {
		const length = this.props.chartEvents.length;
		for (let i = 0; i < length; i++) {
			const event = this.props.chartEvents[i];
			if (event.index === this.props.index) {
				return event;
			}
		}
		return null;
	}

	render() {
		const event = this.findChartEvent();

		if (event === null) {
			return null;
		}

		switch (event.type) {
			case 'expansion':
				return (
					<ArrowUp
						cx={this.props.cx}
						cy={this.props.cy}
					/>
				);
			case 'collapse':
				return (
					<ArrowDown
						cx={this.props.cx}
						cy={this.props.cy}
						collapseType={event.collapseType}
					/>
				);
			default:
				return null;
		}
	}

}

Arrow.propTypes = {
	cx: PropTypes.number,
	cy: PropTypes.number,
	index: PropTypes.number,
	chartEvents: PropTypes.array.isRequired,
};

Arrow.defaultProps = {
	cx: null,
	cy: null,
	index: null,
};


export default Arrow;
