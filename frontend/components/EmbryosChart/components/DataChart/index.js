import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ResponsiveContainer, ComposedChart, Area, Line, ReferenceLine, LabelList, YAxis } from 'recharts';

import Arrow from '../Arrow';

import './styles.scss';

const DEBOUNCE_LIMIT = 100;

const chartMargin = { top: 0, right: 0, bottom: 0, left: 0 };


class DataChart extends PureComponent {

	componentWillUnmount() {
		this.clearTimer();
	}

	moveReferenceLine(value) {
		this.lastUpdate = Date.now();

		const { onReferenceLineChange } = this.props;
		onReferenceLineChange(value || 0);
	}

	clearTimer() {
		if (this.timer) {
			clearTimeout(this.timer);
			delete this.timer;
		}
	}

	onMouseEnterAtChart = (e) => {
		this.moveReferenceLine(e.activeLabel);
	};

	onMouseMoveAtChart = (e) => {
		this.clearTimer();

		const interval = Date.now() - this.lastUpdate;

		if (interval >= DEBOUNCE_LIMIT) {
			this.moveReferenceLine(e.activeLabel);
		} else {
			this.timer = setTimeout(() => {
				this.onMouseMoveAtChart(e);
			}, DEBOUNCE_LIMIT - interval);
		}
	};

	onMouseLeaveChart = () => {
		this.clearTimer();
		this.moveReferenceLine(0);
	};

	render() {
		return (
			<div className="images-chart-container">
				<div className="data-chart-container">
					<ResponsiveContainer height="100%">
						<ComposedChart
							data={this.props.data}
							margin={chartMargin}
							onMouseEnter={this.onMouseEnterAtChart}
							onMouseMove={this.onMouseMoveAtChart}
							onMouseLeave={this.onMouseLeaveChart}
						>
							{/* Expansion Data */}
							<YAxis yAxisId={1} hide={true} domain={[0, 40000]} />
							<Area
								type="monotone"
								dataKey={this.props.dataKey1}
								yAxisId={1}
								isAnimationActive={false}
								stroke={this.props.areaColor}
								fill={this.props.areaColor}
								dot={<Arrow chartEvents={this.props.chartEvents} />}
							/>

							{/* Activity Data */}
							<YAxis yAxisId={2} hide={true} domain={[0, 1]} />
							<Line
								type="monotone"
								dataKey={this.props.dataKey2}
								yAxisId={2}
								stroke={this.props.lineColor}
								isAnimationActive={false}
								dot={false}
							/>

							{/* Divisions Data */}
							<YAxis yAxisId={3} hide={true} scale="log" domain={[1, 256]} />
							<Line
								type="stepAfter"
								dataKey={this.props.dataKey3}
								yAxisId={3}
								stroke="#E37526"
								isAnimationActive={false}
								dot={false}
							>
								<LabelList
									dataKey="division"
									position="top"
									fill="#E37526"
									textAnchor="end"
								/>
							</Line>

							<ReferenceLine x={this.props.embryoReferenceLine} stroke="red" />
						</ComposedChart>
					</ResponsiveContainer>
				</div>
			</div>
		);
	}

}

DataChart.propTypes = {
	data: PropTypes.array.isRequired,
	chartEvents: PropTypes.array.isRequired,

	dataKey1: PropTypes.string.isRequired,
	dataKey2: PropTypes.string.isRequired,
	dataKey3: PropTypes.string.isRequired,

	areaColor: PropTypes.string.isRequired,
	lineColor: PropTypes.string.isRequired,

	embryoReferenceLine: PropTypes.number.isRequired,
	onReferenceLineChange: PropTypes.func.isRequired,
};


export default DataChart;
