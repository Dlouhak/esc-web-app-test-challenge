import React, { Component } from 'react';

import './styles.scss';

class LockCircleIcon extends Component {

	render() {

		return (
			<div className="lock-circle-icon-wrapper">
				<svg width="43px" height="43px" viewBox="0 0 55 50">
					<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-653.000000, -1299.000000)">
						<g id="Group-Copy-2" transform="translate(659.000000, 1303.000000)">
							<g id="Oval-9">
								<use fill="black" fillOpacity="1" filter="url(#filter-2)"></use>
								<use fill="#FFFFFF" fillRule="evenodd"></use>
							</g>
							<g id="Page-1" transform="translate(10.000000, 9.000000)">
								<polygon id="Stroke-1" strokeOpacity="0.00784313771" stroke="#000000" strokeWidth="1.33333335e-11" points="0 0 23.9999985 0 23.9999985 23.9999985 0 23.9999985"></polygon>
								<g id="Group-4" transform="translate(3.000000, 1.000000)" stroke="#B7B7B7" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
									<path d="M1.99999982,9.99999934 L15.9999998,9.99999934 C17.3333337,9.99999934 17.9999998,10.6666662 17.9999998,11.9999993 L17.9999998,18.9999993 C17.9999998,20.3333332 17.3333337,20.9999993 15.9999998,20.9999993 L1.99999982,20.9999993 C0.66666632,20.9999993 -1.8e-07,20.3333332 -1.8e-07,18.9999993 L-1.8e-07,11.9999993 C-1.8e-07,10.6666662 0.66666632,9.99999934 1.99999982,9.99999934 Z" id="Stroke-2"></path>
									<path d="M3.99999958,9.99999934 L3.99999958,5.99999958 C4.00000005,4.07549828 4.83333349,2.63212301 6.50000038,1.6698726 C8.16666813,0.707621948 9.83333503,0.707622298 11.5000019,1.66987355 C13.1666669,2.63212301 13.999999,4.07549828 13.999999,5.99999958 L13.999999,9.99999934" id="Stroke-3"></path>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</div>
		);
	}
}

export default LockCircleIcon;
