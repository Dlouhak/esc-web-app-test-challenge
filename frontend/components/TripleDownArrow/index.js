import React, { Component } from 'react';

class TripleDownArrow extends Component {

	render() {

		return (
			<div className="triple-down-arrow-wrapper">
				<svg width="12px" height="21px">
					<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-674.000000, -1405.000000)">
						<g id="icon-lookdown" transform="translate(668.000000, 1403.000000)">
							<polygon id="Stroke-1" strokeOpacity="0.00784313771" stroke="#000000" strokeWidth="1.33333335e-11" points="0 0 23.9999985 0 23.9999985 23.9999985 0 23.9999985"></polygon>
							<polygon id="Fill-2" fillOpacity="0.4" fill="#5CC6C6" points="7.4099994 2.83999968 11.9999993 7.41999925 16.5899991 2.83999968 17.9999989 4.24999944 11.9999993 10.2499991 5.99999964 4.24999944"></polygon>
							<polygon id="Fill-2-Copy" fillOpacity="0.7" fill="#5CC6C6" points="7.4099994 8.83999968 11.9999993 13.4199993 16.5899991 8.83999968 17.9999989 10.2499994 11.9999993 16.2499991 5.99999964 10.2499994"></polygon>
							<polygon id="Fill-2-Copy-2" fill="#5CC6C6" points="7.4099994 14.8399997 11.9999993 19.4199993 16.5899991 14.8399997 17.9999989 16.2499994 11.9999993 22.2499991 5.99999964 16.2499994"></polygon>
						</g>
					</g>
				</svg>
			</div>
		);
	}

}

export default TripleDownArrow;
