import React from 'react';
import './styles.scss';


export default function GraphIconOne() {
	return (
		<div className="arrow-down-graph-wrapper">
			<svg width="19px" height="12px" viewBox="0 0 19 12">
				<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-547.000000, -829.000000)">
					<polygon id="icon-graph1" fill="#BDE5F8" points="547.875083 841 550.898253 834.110558 557.597309 834.110558 559.488685 829 565.875083 829 565.875083 841" />
				</g>
			</svg>
		</div>
	);
};
