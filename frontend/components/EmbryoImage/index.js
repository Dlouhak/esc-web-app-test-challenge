import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

class EmbryoImage extends PureComponent {

	render() {
		const imageCount = +this.props.embryo.image_count;
		const backgroundFrame = Math.max(0, Math.min(imageCount - 1, this.props.currentSelectedEmbryoReferenceLine - this.props.offset));

		return (
			<div className="embryo-image-over-graph">
				<div className="bg-img" style={{
					opacity: 1.0,
					backgroundImage: `url(${this.props.embryo.image_url})`,
					backgroundPositionX: `-${backgroundFrame * 250}px`
				}} />
			</div>
		);
	}

}

EmbryoImage.propTypes = {
	embryo: PropTypes.object,
	offset: PropTypes.number,
	currentSelectedEmbryoReferenceLine: PropTypes.number,
};

EmbryoImage.defaultProps = {
	offset: 0,
	currentSelectedEmbryoReferenceLine: null,
};

export default EmbryoImage;
