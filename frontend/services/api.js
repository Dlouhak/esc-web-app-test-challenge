import fetch from 'isomorphic-fetch';

import mockup_embryos_library from './mockup_embryos_library.json';
import mockup_more_embryos from './mockup_more_embryos.json';

import mockup_learning from './mockup_learning.json';

import mockup_user_progress from './mockup_training_user_progress_response.json';

import mockup_training_embryo_training from './mockup_training_embryo_training_response.json';
import mockup_training_embryo_training_result from './mockup_training_embryo_training_result_response.json';

import mockup_training_embryo_test from './mockup_training_embryo_test_response.json';
import mockup_training_embryo_test_result from './mockup_training_embryo_test_result_response.json';

/* Embryos Library */

export async function mockupFetchEmbryosLibrary() {

	return mockup_embryos_library;

}

export async function fetchEmbryoDetail(embryoId) {
	const url = `${process.env.PUBLIC_URL}/api/embryo-detail/${embryoId}`;
	const response = await fetch(url, {
		credentials: 'same-origin',
		method: 'get',
		headers: {
			'Content-Type': 'application/json'
		}
	});

	return await response.json();
}

export async function fetchEmbryosLibrary(limit = 10, embryoId = null) {
	const query = `?limit=${limit}${embryoId !== null ? `&embryoId=${embryoId}` : ''}`;

	const response = await fetch(`${process.env.PUBLIC_URL}/api/embryo${query}`, {
		credentials: 'same-origin',
	});
	return await response.json();

}


export async function mockupFetchMoreEmbryos(cluster, offset = 0, limit = 10) {

	return mockup_more_embryos;

}

export async function fetchMoreEmbryos(cluster, offset = 0, limit = 10) {

	const url = `${process.env.PUBLIC_URL}/api/embryo/` + cluster + "?limit=" + limit + "&offset=" + offset;
	const response = await fetch(url, {
		credentials: 'same-origin',
	});
	return await response.json();

}


/* Learning */

export async function mockupFetchLearning() {

	return mockup_learning;

}

export async function fetchLearning() {

	const response = await fetch(`${process.env.PUBLIC_URL}/api/learning`, {
		credentials: 'same-origin',
	});
	return await response.json();

}

export async function visitLearning(section) {

	const response = await fetch(`${process.env.PUBLIC_URL}/api/learning`, {
		credentials: 'same-origin',
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({ section })
	});
	return await response.json();

}


/* Training */

export async function mockupFetchUserProgress() {

	const data = mockup_user_progress;
	return data;

}

export async function fetchUserProgress(levelValue) {

	const levelParameter = (levelValue === undefined) ? "" : "?level=" + levelValue;
	const url = `${process.env.PUBLIC_URL}/api/training` + levelParameter;

	const response = await fetch(url, {

		credentials: 'same-origin',
		method: 'get',
		headers: {
			'Content-Type': 'application/json'
		}

	});

	return await response.json();

}



export async function mockupFetchEmbryosTraining() {

	const data = mockup_training_embryo_training;
	return data;

}

export async function fetchEmbryosTraining(levelValue) {

	let levelJSON = {};
	levelJSON["level"] = levelValue;

	const levelString = JSON.stringify(levelJSON);

	const response = await fetch(`${process.env.PUBLIC_URL}/api/training`, {

		credentials: 'same-origin',
		method: 'post',
		 headers: {
			'Content-Type': 'application/json'
		},
		body: levelString

	});

	return await response.json();

}

export async function fetchEmbryosTrainingResult(id, answer) {

	const response = await fetch(`${process.env.PUBLIC_URL}/api/training/` + id, {

		credentials: 'same-origin',
		method: 'put',
		 headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({ answer })

	});

	return await response.json();

}


export async function mockupFetchEmbryosTrainingResult() {

	const data = mockup_training_embryo_training_result;
	return data;

}



export async function mockupFetchEmbryosTest() {

	const data = mockup_training_embryo_test;
	return data;

}

export async function fetchEmbryosTest() {

	const response = await fetch(`${process.env.PUBLIC_URL}/api/test`, {

		credentials: 'same-origin',
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: "{}"

	});

	return await response.json();

}


export async function mockupFetchEmbryosTestResult() {

	const data = mockup_training_embryo_test_result;
	return data;

}

export async function fetchEmbryosTestResult(id, answers) {

	const response = await fetch(`${process.env.PUBLIC_URL}/api/test/` + id, {

		credentials: 'same-origin',
		method: 'put',
		 headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({ "answers": answers })

	});

	return await response.json();

}
