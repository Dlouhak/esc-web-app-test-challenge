<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Update 7d and 8d clusters name and short name
 */
class Version20180205225632 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Arrest\', `short_name` = \'Arrest\', `name` = \'Arrested/Degenerated\' WHERE `id` = 30');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Unfertil\', `short_name` = \'Unfertil\', `name` = \'Unfertilized/Uncleaved\' WHERE `id` = 31');
	}


	public function down(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Arrested\', `short_name` = \'Arrested\', `name` = \'Arrested/degenerated\' WHERE `id` = 30');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Unfertilized\', `short_name` = \'Unfertilized\', `name` = \'Unfertilized\' WHERE `id` = 31');
	}
}
