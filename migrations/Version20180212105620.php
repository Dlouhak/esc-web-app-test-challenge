<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Add table training_question, add level to user
 */
class Version20180212105620 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE training_question (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, embryo_id INT NOT NULL, level VARCHAR(255) NOT NULL, answer VARCHAR(255) DEFAULT NULL, correct TINYINT(1) DEFAULT NULL, answered_at DATETIME DEFAULT NULL, INDEX IDX_51925AD9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql('ALTER TABLE training_question ADD CONSTRAINT FK_51925AD9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE user ADD level VARCHAR(255) DEFAULT \'newUser\' NOT NULL');
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE training_question');
		$this->addSql('ALTER TABLE user DROP level');
	}
}
