<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Create tables test and test_question
 */
class Version20180216173626 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, level VARCHAR(255) NOT NULL, passed TINYINT(1) DEFAULT NULL, completed_at DATETIME DEFAULT NULL, INDEX IDX_D87F7E0CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql('CREATE TABLE test_question (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, embryo_id INT NOT NULL, answer VARCHAR(255) DEFAULT NULL, correct TINYINT(1) DEFAULT NULL, `order` SMALLINT NOT NULL, INDEX IDX_239442181E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE test_question ADD CONSTRAINT FK_239442181E5D0459 FOREIGN KEY (test_id) REFERENCES test (id) ON DELETE CASCADE');
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE test_question DROP FOREIGN KEY FK_239442181E5D0459');
		$this->addSql('DROP TABLE test');
		$this->addSql('DROP TABLE test_question');
	}
}
