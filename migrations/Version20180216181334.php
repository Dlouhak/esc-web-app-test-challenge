<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Add five booleans to User to track visited learning pages
 */
class Version20180216181334 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE user ADD introduction_visited TINYINT(1) DEFAULT \'0\' NOT NULL, ADD stages_of_development_visited TINYINT(1) DEFAULT \'0\' NOT NULL, ADD embryo_quality_visited TINYINT(1) DEFAULT \'0\' NOT NULL, ADD events_visited TINYINT(1) DEFAULT \'0\' NOT NULL, ADD implantation_chance_visited TINYINT(1) DEFAULT \'0\' NOT NULL');
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE user DROP introduction_visited, DROP stages_of_development_visited, DROP embryo_quality_visited, DROP events_visited, DROP implantation_chance_visited');
	}
}
