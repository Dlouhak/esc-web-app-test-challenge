<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Update cluster data
 */
class Version20180122155949 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `short_name` = \'HBlast LowCell\' WHERE `id` = 6');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'HBlast Deg\' WHERE `id` = 7');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExxBlast LowCell\' WHERE `id` = 14');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExxBlast Deg\' WHERE `id` = 15');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExBlast LowCell\' WHERE `id` = 18');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExBlast Deg\' WHERE `id` = 19');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Blast LowCell\' WHERE `id` = 22');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Blast Deg\' WHERE `id` = 23');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Early Blast\' WHERE `id` = 24');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Early Blast\', `short_name` = \'Early Blast LowCell\' WHERE `id` = 25');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'Early Blast\', `short_name` = \'Early Blast Deg\' WHERE `id` = 26');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Morula LowCell\' WHERE `id` = 28');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Morula Deg\' WHERE `id` = 29');
		$this->addSql('UPDATE `cluster` SET `classification` = \'8da\' WHERE `id` = 31');
	}


	public function down(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `short_name` = \'HBlast low cells\' WHERE `id` = 6;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'HBlast deg\' WHERE `id` = 7;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExxBlast low cells\' WHERE `id` = 14;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExxBlast deg\' WHERE `id` = 15;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExBlast low cells\' WHERE `id` = 18;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'ExBlast deg\' WHERE `id` = 19;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Blast low cells\' WHERE `id` = 22;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Blast deg\' WHERE `id` = 23;');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'EBlast\' WHERE `id` = 24;');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'EBlast\', `short_name` = \'EBlast low cells\' WHERE `id` = 25;');
		$this->addSql('UPDATE `cluster` SET `development_phase` = \'EBlast\', `short_name` = \'EBlast deg\' WHERE `id` = 26;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Morula low cells\' WHERE `id` = 28;');
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Morula deg\' WHERE `id` = 29;');
		$this->addSql('UPDATE `cluster` SET `classification` = \'7dd\' WHERE `id` = 31;');
	}
}
