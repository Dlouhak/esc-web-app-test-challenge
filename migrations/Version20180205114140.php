<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * create User Password Code table
 */
class Version20180205114140 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE user_password_code (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, code_hash VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_BCE9A83A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql('ALTER TABLE user_password_code ADD CONSTRAINT FK_BCE9A83A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE user_password_code');
	}
}
