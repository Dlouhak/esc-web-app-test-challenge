<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Change Clinic table
 */
class Version20180205193927 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE clinic ADD import_id INT NOT NULL, ADD email VARCHAR(255) NOT NULL, ADD latitude NUMERIC(10, 7) DEFAULT NULL, ADD longitude NUMERIC(10, 7) DEFAULT NULL, ADD updated_at DATETIME NOT NULL, DROP city, DROP country, DROP address');
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE clinic ADD country VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD address VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP import_id, DROP latitude, DROP longitude, DROP updated_at, CHANGE email city VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
	}
}
