<?php
declare(strict_types=1);

namespace Netvor\Embryo\Components;

use Nette\Application\UI;
use Nette\Application\UI\Form;
use Netvor\Embryo\Mails\MailService;


class ContactControl extends UI\Control
{
	/** @var MailService*/
	private $mailService;

	/** @var string */
	private $mailTo;


	public function __construct(string $mailTo, MailService $mailService)
	{
		parent::__construct();
		$this->mailService = $mailService;
		$this->mailTo = $mailTo;
	}


	public function render(): void
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/ContactControl.latte');
		$template->render();
	}


	public function createComponentContactForm(): Form
	{
		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again');
		$form->addText('fullName', 'Full name')
			->setRequired('Please enter your name');
		$form->addText('email', 'E-mail')
			->setRequired('Please enter a valid e-mail address')
			->addRule(Form::EMAIL, 'Please enter a valid e-mail address');
		$form->addTextArea('message', 'Message')
			->setRequired('Please enter a message');
		$form->addSubmit('submit', 'Save Message');

		$form->onSuccess[] = [$this, 'contactFormSucceeded'];
		return $form;
	}


	public function contactFormSucceeded($form, array $values): void
	{
		$addReply = function (\Nette\Mail\Message $mail) use ($values) {
			$mail->addReplyTo($values['email']);
		};

		$this->mailService->send('contact', $this->mailTo, $values, $addReply);
		$this->flashMessage('Your message was successfuly sent.');
		$this->presenter->redirect('this#contact-form');
	}
}
