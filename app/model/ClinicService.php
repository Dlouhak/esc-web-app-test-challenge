<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;
use Netvor\Embryo\Model\Entities\Clinic;


class ClinicService
{
	use Nette\SmartObject;

	/** @var EntityRepository */
	private $repository;

	/** @var EntityManager */
	private $entityManager;


	public function __construct(EntityManager $entityManager)
	{
		/** @var EntityRepository $clinicRepository */
		$clinicRepository = $entityManager->getRepository(Clinic::class);
		$this->entityManager = $entityManager;
		$this->repository = $clinicRepository;
	}


	public function get(?int $id): ?Clinic
	{
		return $id !== null ? $this->repository->find($id) : null;
	}


	/**
	 * @return Clinic[]
	 */
	public function getAll(?array $criteria = null): array
	{
		return $criteria !== null ?
			$this->repository->findBy($criteria) :
			$this->repository->findAll();
	}


	public function create(int $importId, string $name, string $email, ?string $latitude, ?string $longitude, Nette\Utils\DateTime $updatedAt): Clinic
	{
		$clinic = new Clinic($importId, $name, $email, $latitude, $longitude, $updatedAt);
		$this->entityManager->persist($clinic);
		$this->entityManager->flush();
		return $clinic;
	}
}
