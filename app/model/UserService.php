<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;
use Netvor\Embryo\Mails\MailService;
use Netvor\Embryo\Model\Entities\Clinic;
use Netvor\Embryo\Model\Entities\User;


class UserService
{
	use Nette\SmartObject;

	/** @var MailService */
	private $mailService;

	/** @var UserAuthService */
	private $userAuthService;

	/** @var EntityManager */
	private $entityManager;

	/** @var EntityRepository */
	private $repository;


	public function __construct(MailService $mailService, EntityManager $entityManager, UserAuthService $userAuthService)
	{
		/** @var EntityRepository $userRepository */
		$userRepository = $entityManager->getRepository(User::class);
		$this->mailService = $mailService;
		$this->entityManager = $entityManager;
		$this->repository = $userRepository;
		$this->userAuthService = $userAuthService;
	}


	public function get($id): ?User
	{
		return $id !== null ? $this->repository->find($id) : null;
	}


	public function getByEmail($email): ?User
	{
		return $this->userAuthService->getByEmail($email);
	}


	/**
	 * @return array of Entities\User
	 */
	public function getAll(?array $criteria = null): array
	{
		return $criteria !== null ?
			$this->repository->findBy($criteria) :
			$this->repository->findAll();
	}


	public function create(string $firstName, string $lastName, string $email, string $password, Clinic $clinic = null): User
	{
		if ($this->repository->countBy(['email' => $email]) > 0) {
			throw new EmailAlreadyInUseException;
		}
		$user = new User($firstName, $lastName, $email, $password, $clinic);

		$this->mailService->send('registration', $email, ['newUser' => $user, 'feedbackMail' => 'feedback@embryoscoring.net']);

		$this->entityManager->persist($user);
		$this->entityManager->flush();
		return $user;
	}


	public function update(User $user, Clinic $clinic = null, string $firstName = null, string $lastName = null, string $password = null): void
	{
		if ($password !== null) {
			$user->setPassword($password);
		}
		if ($firstName !== null) {
			$user->setFirstName($firstName);
		}
		if ($lastName !== null) {
			$user->setLastName($lastName);
		}
		$user->setClinic($clinic);
		$this->entityManager->flush();
	}


	public function visitLearning(User $user, string $section)
	{
		$user->visitLearning($section);
		$this->entityManager->flush();
	}
}


class EmailAlreadyInUseException extends \Exception
{
}
