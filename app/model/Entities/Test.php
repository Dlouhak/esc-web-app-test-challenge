<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\Common\Collections;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Nette;


/**
 * @ORM\Entity
 * @property-read ?int $id
 * @property-read User $user
 * @property-read Collections\Collection $questions
 * @property-read string $level one of the TestQuestion::LEVELS
 * @property-read ?bool $passed
 * @property-read ?Nette\Utils\DateTime $completedAt
 * @property-read bool $finished
 */
class Test
{
	use Nette\SmartObject;

	public const QUESTION_COUNT = 6;

	public const SCORE_THRESHOLD = 5;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @var Collections\Collection
	 * @ORM\OneToMany(targetEntity="TestQuestion", mappedBy="test", indexBy="id", cascade={"persist"})
	 * @ORM\OrderBy({"order" = "ASC"})
	 */
	private $questions;

	/**
	 * @ORM\Column
	 * @var string $level one of the TestQuestion::LEVELS
	 */
	private $level;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 * @var ?bool
	 */
	private $passed;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var ?\DateTime
	 */
	private $completedAt;


	public function __construct(User $user, array $embryoIds)
	{
		$level = $user->getNextLevel();
		if ($level === null) {
			throw new \InvalidArgumentException;
		}

		$this->user = $user;
		$this->level = $level;

		if (count($embryoIds) !== self::QUESTION_COUNT || !Nette\Utils\Arrays::every($embryoIds, function ($id) {
			return is_int($id);
		})) {
			throw new \InvalidArgumentException;
		}

		$this->questions = new Collections\ArrayCollection;
		for ($i = 0; $i < self::QUESTION_COUNT; $i++) {
			$this->questions->add(new TestQuestion($this, $embryoIds[$i], $i + 1));
		}
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	public function getUser(): User
	{
		return $this->user;
	}


	public function getQuestions(): Collections\Collection
	{
		return new ReadOnlyCollectionWrapper($this->questions);
	}


	/**
	 * @return string one of the TestQuestion::LEVELS
	 */
	public function getLevel(): string
	{
		return $this->level;
	}


	public function isPassed(): ?bool
	{
		return $this->passed;
	}


	public function getCompletedAt(): ?Nette\Utils\DateTime
	{
		return $this->completedAt !== null ? Nette\Utils\DateTime::from($this->completedAt) : null;
	}


	public function isFinished(): bool
	{
		return $this->passed !== null;
	}


	/**
	 * @param string[] $answers
	 * @param string[] $correctClassifications
	 * @return $this
	 */
	public function take(array $answers, array $correctClassifications): self
	{
		$keys = $this->questions->getKeys();
		$count = $this->questions->count();

		if (
			count($answers) !== $count ||
			count($correctClassifications) !== $count ||
			!Nette\Utils\Arrays::every($keys, function ($key) use ($answers, $correctClassifications) {
				return isset($answers[$key], $correctClassifications[$key]) && is_string($answers[$key]) && is_string($correctClassifications[$key]);
			})
		) {
			throw new \InvalidArgumentException;
		}

		Nette\Utils\Arrays::map($keys, function ($key) use ($answers, $correctClassifications) {
			$this->questions[$key]->answer($answers[$key], $correctClassifications[$key]);
		});

		$correct = $this->questions->filter(function (TestQuestion $question) {
			return $question->isCorrect() === true;
		})->count();

		$this->passed = $correct >= self::SCORE_THRESHOLD;
		$this->completedAt = new Nette\Utils\DateTime;

		return $this;
	}
}
