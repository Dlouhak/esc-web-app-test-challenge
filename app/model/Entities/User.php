<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;


/**
 * @ORM\Entity
 * @property-read ?int $id
 * @property ?Clinic $clinic
 * @property string $firstName
 * @property string $lastName
 * @property-read string $fullName
 * @property string $email
 * @property-write string $password
 * @property-read string $level one of the self::LEVELS
 * @property-read ?string $nextLevel one of the self::LEVELS or null if already at max level
 * @property bool $introductionVisited
 * @property bool $stagesOfDevelopmentVisited
 * @property bool $embryoQualityVisited
 * @property bool $eventsVisited
 * @property bool $implantationChanceVisited
 */
class User
{
	use Nette\SmartObject;

	public const LEVEL_NEW_USER = 'newUser';

	public const LEVEL_BEGINNER = 'beginner';

	public const LEVEL_INTERMEDIATE = 'intermediate';

	public const LEVEL_EXPERT = 'expert';

	public const LEVELS = [
		self::LEVEL_NEW_USER,
		self::LEVEL_BEGINNER,
		self::LEVEL_INTERMEDIATE,
		self::LEVEL_EXPERT,
	];

	public const LEARNING_INTRODUCTION = 'introduction';

	public const LEARNING_STAGES_OF_DEVELOPMENT = 'stagesOfDevelopment';

	public const LEARNING_EMBRYO_QUALITY = 'embryoQuality';

	public const LEARNING_EVENTS = 'events';

	public const LEARNING_IMPLANTATION_CHANCE = 'implantationChance';

	public const LEARNING_SECTIONS = [
		self::LEARNING_INTRODUCTION,
		self::LEARNING_STAGES_OF_DEVELOPMENT,
		self::LEARNING_EMBRYO_QUALITY,
		self::LEARNING_EVENTS,
		self::LEARNING_IMPLANTATION_CHANCE,
	];

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Clinic")
	 * @var ?Clinic
	 */
	private $clinic;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $firstName;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $lastName;

	/**
	 * @ORM\Column(unique=true)
	 * @var string
	 */
	private $email;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $passwordHash;

	/**
	 * @ORM\Column(options={"default": User::LEVEL_NEW_USER})
	 * @var string one of the self::LEVELS
	 */
	private $level = self::LEVEL_NEW_USER;

	/**
	 * @ORM\Column(type="boolean", options={"default": false})
	 * @var bool
	 */
	private $introductionVisited = false;

	/**
	 * @ORM\Column(type="boolean", options={"default": false})
	 * @var bool
	 */
	private $stagesOfDevelopmentVisited = false;

	/**
	 * @ORM\Column(type="boolean", options={"default": false})
	 * @var bool
	 */
	private $embryoQualityVisited = false;

	/**
	 * @ORM\Column(type="boolean", options={"default": false})
	 * @var bool
	 */
	private $eventsVisited = false;

	/**
	 * @ORM\Column(type="boolean", options={"default": false})
	 * @var bool
	 */
	private $implantationChanceVisited = false;


	public function __construct(string $firstName, string $lastName, string $email, string $password, Clinic $clinic = null)
	{
		$this->setClinic($clinic);
		$this->setFirstName($firstName);
		$this->setLastName($lastName);
		$this->setEmail($email);
		$this->setPassword($password);
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	public function getClinic(): ?Clinic
	{
		return $this->clinic;
	}


	/**
	 * @return $this
	 */
	public function setClinic(?Clinic $clinic = null): self
	{
		$this->clinic = $clinic;
		return $this;
	}


	public function getFirstName(): string
	{
		return $this->firstName;
	}


	/**
	 * @return $this
	 */
	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;
		return $this;
	}


	public function getLastName(): string
	{
		return $this->lastName;
	}


	/**
	 * @return $this
	 */
	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;
		return $this;
	}


	public function getFullName(): string
	{
		return $this->getFirstName() . ' ' . $this->getLastName();
	}


	public function getEmail(): string
	{
		return $this->email;
	}


	/**
	 * @return $this
	 */
	public function setEmail(string $email): self
	{
		if (!Nette\Utils\Validators::isEmail($email)) {
			throw new \InvalidArgumentException;
		}

		$this->email = $email;
		return $this;
	}


	/**
	 * @return $this
	 */
	public function setPassword(string $password): self
	{
		if ($password === '') {
			throw new \InvalidArgumentException;
		}

		$this->passwordHash = Nette\Security\Passwords::hash($password);
		return $this;
	}


	public function verifyPassword(string $password): bool
	{
		return Nette\Security\Passwords::verify($password, $this->passwordHash);
	}


	/**
	 * @return string one of the self::LEVELS
	 */
	public function getLevel(): string
	{
		return $this->level;
	}


	/**
	 * @return ?string one of the self::LEVELS or null if already at max level
	 */
	public function getNextLevel(): ?string
	{
		switch ($this->level) {
			case self::LEVEL_NEW_USER:
				return self::LEVEL_BEGINNER;
			case self::LEVEL_BEGINNER:
				return self::LEVEL_INTERMEDIATE;
			case self::LEVEL_INTERMEDIATE:
				return self::LEVEL_EXPERT;
			default:
				return null;
		}
	}


	/**
	 * @return $this
	 */
	public function levelUp(): self
	{
		$nextLevel = $this->getNextLevel();
		if ($nextLevel === null) {
			throw new \RuntimeException;
		}
		$this->level = $nextLevel;
		return $this;
	}


	public function isIntroductionVisited(): bool
	{
		return $this->introductionVisited;
	}


	public function isStagesOfDevelopmentVisited(): bool
	{
		return $this->stagesOfDevelopmentVisited;
	}


	public function isEmbryoQualityVisited(): bool
	{
		return $this->embryoQualityVisited;
	}


	public function isEventsVisited(): bool
	{
		return $this->eventsVisited;
	}


	public function isImplantationChanceVisited(): bool
	{
		return $this->implantationChanceVisited;
	}


	/**
	 * @return $this
	 */
	public function visitLearning(string $section): self
	{
		switch ($section) {
			case self::LEARNING_INTRODUCTION:
				$this->introductionVisited = true;
				break;
			case self::LEARNING_STAGES_OF_DEVELOPMENT:
				$this->stagesOfDevelopmentVisited = true;
				break;
			case self::LEARNING_EMBRYO_QUALITY:
				$this->embryoQualityVisited = true;
				break;
			case self::LEARNING_EVENTS:
				$this->eventsVisited = true;
				break;
			case self::LEARNING_IMPLANTATION_CHANCE:
				$this->implantationChanceVisited = true;
				break;
			default:
				throw new \InvalidArgumentException;
		}
		return $this;
	}
}
