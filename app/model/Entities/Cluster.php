<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;


/**
 * @ORM\Entity
 * @property-read ?int $id
 * @property-read int $catiId
 * @property-read string $classification
 * @property-read string $developmentPhase
 * @property-read string $shortName
 * @property-read string $name
 * @property-read string $description
 * @property-read int $implantationChance
 */
class Cluster
{
	use Nette\SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer", unique=true)
	 * @var int
	 */
	private $catiId;

	/**
	 * @ORM\Column(unique=true)
	 * @var string
	 */
	private $classification;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $developmentPhase;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $shortName;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\Column(type="smallint")
	 * @var int
	 */
	private $implantationChance;


	public function __construct(int $catiId, string $classification, string $developmentPhase, string $shortName, string $name, string $description, int $implantationChance)
	{
		$this->catiId = $catiId;
		$this->classification = $classification;
		$this->developmentPhase = $developmentPhase;
		$this->shortName = $shortName;
		$this->name = $name;
		$this->description = $description;
		$this->implantationChance = $implantationChance;
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	public function getCatiId(): int
	{
		return $this->catiId;
	}


	public function getClassification(): string
	{
		return $this->classification;
	}


	public function getDevelopmentPhase(): string
	{
		return $this->developmentPhase;
	}


	public function getShortName(): string
	{
		return $this->shortName;
	}


	public function getName(): string
	{
		return $this->name;
	}


	public function getDescription(): string
	{
		return $this->description;
	}


	public function getImplantationChance(): int
	{
		return $this->implantationChance;
	}
}
