<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;


/**
 * @ORM\Entity
 * @property-read ?int $id
 * @property-read int $importId
 * @property-read string $name
 * @property-read string $email
 * @property-read ?string $latitude
 * @property-read ?string $longitude
 * @property-read Nette\Utils\DateTime $updatedAt
 */
class Clinic
{
	use Nette\SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $importId;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $email;

	/**
	 * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
	 * @var ?string
	 */
	private $latitude;

	/**
	 * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
	 * @var ?string
	 */
	private $longitude;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $updatedAt;


	public function __construct(int $importId, string $name, string $email, ?string $latitude, ?string $longitude, Nette\Utils\DateTime $updatedAt)
	{
		$this->importId = $importId;
		$this->name = $name;
		$this->email = $email;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
		$this->updatedAt = $updatedAt;
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	public function getImportId(): int
	{
		return $this->importId;
	}


	public function getName(): string
	{
		return $this->name;
	}


	public function getEmail(): string
	{
		return $this->email;
	}


	public function getLatitude(): ?string
	{
		return $this->latitude;
	}


	public function getLongitude(): ?string
	{
		return $this->longitude;
	}


	public function getUpdatedAt(): Nette\Utils\DateTime
	{
		return Nette\Utils\DateTime::from($this->updatedAt);
	}
}
