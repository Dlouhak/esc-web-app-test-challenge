<?php
declare(strict_types=1);

namespace Netvor\Embryo\Console;

use Nette;
use Netvor\Embryo\Model\ClinicService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ImportClinicsCommand extends Command
{
	public const AUTH_URL = 'https://ivf-stag.utdigit.com/oauth/v2/token';

	public const CLINIC_URL = 'https://ivf-stag.utdigit.com/clinics-export';

	public const CLIENT_ID = 'EscapeClient';

	public const CLIENT_SECRET = '23i2umqloz28sskg8csc4gsgg4ccgog8gokgc80oog844wwgws';

	public const GRANT_TYPE = 'client_credentials';

	/** @var ClinicService */
	private $clinicModel;


	public function __construct(ClinicService $clinicService)
	{
		parent::__construct();
		$this->clinicModel = $clinicService;
	}


	protected function configure(): void
	{
		$this->setName('clinics:import');
		$this->setDescription('Import clinics from fertilitypedia.');
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$data = [
			'client_id' => $this::CLIENT_ID,
			'client_secret' => $this::CLIENT_SECRET,
			'grant_type' => $this::GRANT_TYPE,
		];

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this::AUTH_URL);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = Nette\Utils\Json::decode($response, Nette\Utils\Json::FORCE_ARRAY);


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this::CLINIC_URL . '?access_token=' . $response['access_token']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$clinics = Nette\Utils\Json::decode($response, Nette\Utils\Json::FORCE_ARRAY);

		foreach ($clinics['data'] as $clinic) {
			$this->clinicModel->create($clinic['id'], $clinic['name'], $clinic['email'], $clinic['latitude'], $clinic['longitude'], new Nette\Utils\DateTime($clinic['updated_at']));
		}
		return 0;
	}
}
