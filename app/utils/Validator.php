<?php
declare(strict_types=1);

namespace Netvor\Embryo\Utils;

use Nette;
use Nette\Utils\Validators;


class Validator
{
	use Nette\StaticClass;

	public const ERROR_STRING_LENGTH = 40;


	/**
	 * @param string|array $expected
	 * @return mixed|null
	 */
	public static function validateField(array $data, string $field, $expected, array &$errors = null, bool $required = true)
	{
		if ($errors === null) {
			$errors = [];
		}

		if (!isset($data[$field])) {
			if ($required) {
				$errors[] = sprintf('The field \'%s\' is missing.', $field);
			}
			return null;
		}

		$value = $data[$field];
		foreach ((array) $expected as $validator) {
			if (is_array($validator)) {
				if (!in_array($value, $validator, true)) {
					$allowed = self::formatAllowedValues($validator);
					$given = self::formatGivenValue($value);
					$errors[] = sprintf('The field \'%s\' expects to be one of valid options [%s], %s given.', $field, $allowed, $given);
					return null;
				}
			} else {
				try {
					Validators::assert($value, $validator, sprintf('field \'%s\'', $field));
				} catch (Nette\Utils\AssertionException $e) {
					if ($validator === 'url' && Validators::is($value = 'http://' . $value, $validator)) {
						continue;
					}
					$errors[] = $e->getMessage();
					return null;
				}
			}
		}

		return $value;
	}


	public static function validateFile(array $data, string $field, array &$errors = null, bool $required = true, array $types = null): ?Nette\Http\FileUpload
	{
		if ($errors === null) {
			$errors = [];
		}

		$file = Nette\Utils\Arrays::get($data, $field, null);
		if ($file === null || !$file instanceof Nette\Http\FileUpload || !$file->hasFile()) {
			if ($required) {
				$errors[] = sprintf('The file \'%s\' is missing.', $field);
			}
			return null;
		}

		if (!$file->isOk()) {
			$errors[] = sprintf('The file \'%s\' was not uploaded successfully.', $field);
			return null;
		}
		if ($types !== null && !in_array($type = $file->getContentType(), $types, true)) {
			$allowed = self::formatAllowedValues($types);
			$given = !(is_string($type) && strlen($type) > self::ERROR_STRING_LENGTH) ?
				sprintf(', %s given', self::formatGivenValue($type)) :
				'';
			$errors[] = sprintf('The type of file \'%s\' expects to be one of valid options [%s]%s.', $field, $allowed, $given);
			return null;
		}

		return $file;
	}


	private static function formatAllowedValues(array $allowed): string
	{
		$set = array_map(function ($s) { return var_export($s, true); }, $allowed);
		return Nette\Utils\Strings::truncate(implode(', ', $set), self::ERROR_STRING_LENGTH, '...');
	}


	/**
	 * @param mixed $value
	 */
	private static function formatGivenValue($value): string
	{
		if (is_array($value)) {
			return sprintf('array(%d)', count($value));
		}
		if (is_object($value)) {
			return sprintf('object %s', get_class($value));
		}
		if (is_string($value) && strlen($value) <= self::ERROR_STRING_LENGTH) {
			return sprintf('string \'%s\'', $value);
		}
		return gettype($value);
	}
}
