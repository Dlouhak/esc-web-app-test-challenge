<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Model;

use Kdyby;
use Nette;
use Netvor\Embryo\Model\Entities\Cluster;


class EmbryoService
{
	use Nette\SmartObject;

	public const DEFAULT_LIMIT = 5;

	public const COLLAPSE_SCAN_LIMIT = 3;

	public const COLLAPSE_CLASS_LIMITS = [
		'veryExtensive' => 60,
		'extensive' => 80,
		'mild' => 90,
		'veryMild' => 100,
	];

	public const MINUTES_PER_FRAME = 30;

	/** @var Kdyby\Doctrine\EntityRepository */
	private $clusterRepository;

	/** @var Kdyby\Doctrine\Connection */
	private $catiDatabase;

	/** @var string */
	private $catiVersion;

	/** @var string */
	private $imageDir;


	public function __construct(Kdyby\Doctrine\EntityManager $entityManager, Kdyby\Doctrine\Connection $catiDatabase, string $catiVersion, string $imageDir)
	{
		/** @var Kdyby\Doctrine\EntityRepository $clusterRepository */
		$clusterRepository = $entityManager->getRepository(Cluster::class);
		$this->clusterRepository = $clusterRepository;
		$this->catiDatabase = $catiDatabase;
		$this->catiVersion = $catiVersion;
		$this->imageDir = $imageDir;
	}


	public function getCluster(string $clusterId): ?Cluster
	{
		return $this->clusterRepository->findOneBy(['classification' => $clusterId]);
	}


	public function getClusterByCatiId(string $catiId): ?Cluster
	{
		return $this->clusterRepository->findOneBy(['catiId' => $catiId]);
	}


	/**
	 * @return Cluster[]
	 */
	public function getClusters(): array
	{
		return $this->clusterRepository->findBy([], ['classification' => 'ASC']);
	}


	/**
	 * @return string[]
	 */
	public function getClusterColors(): array
	{
		return Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/colors.neon'));
	}


	public function getClusterCategories(): array
	{
		return Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/categories.neon'));
	}


	public function getLearningDescriptions(): array
	{
		return Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/learning.neon'));
	}


	public function getEmbryoById(int $id): ?array
	{
		return $this->queryEmbryos(['ids' => [$id]])->fetch() ?: null;
	}


	/**
	 * @param int[] $ids
	 * @return array[]
	 */
	public function getEmbryosByIds(array $ids): array
	{
		$embryos = $this->queryEmbryos(['ids' => $ids])->fetchAll();
		/** @var array[] $embryosById */
		$embryosById = Nette\Utils\Arrays::associate($embryos, 'EmbryoId=');
		return $embryosById;
	}


	/**
	 * @param Cluster $cluster
	 * @param int|null $offset
	 * @param int|null $limit
	 * @return array[]
	 */
	public function getEmbryosByCluster(Cluster $cluster, int $offset = null, int $limit = null): array
	{
		return $this->queryEmbryos(['cluster' => $cluster], $offset ?? 0, $limit ?? self::DEFAULT_LIMIT)
			->fetchAll();
	}


	/**
	 * @param int[] $clusterCatiIds
	 * @param int|null $limit
	 * @return array[]
	 */
	public function getEmbryosByClusters(array $clusterCatiIds, int $limit = null, int $embryoId = null): array
	{
		$allEmbryos = $this->catiDatabase->executeQuery('
			SELECT CATIResultsEmbryos.EmbryoId, Embryos.CurveClass
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
			AND Embryos.CurveClass IN (:clusters)
			ORDER BY Embryos.CurveClass, CATIResultsEmbryos.EmbryoId
		', [
			'version' => $this->catiVersion,
			'clusters' => $clusterCatiIds,
		], [
			'clusters' => Kdyby\Doctrine\Connection::PARAM_INT_ARRAY,
		])->fetchAll();

		/** @var array $idsByCluster */
		$idsByCluster = Nette\Utils\Arrays::associate($allEmbryos, 'CurveClass[]=EmbryoId');
		$found = false;
		$ids = array_merge(...Nette\Utils\Arrays::map($idsByCluster, function (array $clusterIds) use ($limit, $embryoId, &$found) {
			$pages = 1;
			if (!$found && $embryoId !== null) {
				$key = array_search((string) $embryoId, $clusterIds, true);
				if ($key !== false) {
					$found = true;
					$pages = floor($key / ($limit ?? self::DEFAULT_LIMIT)) + 1;
				}
			}
			return array_slice($clusterIds, 0, $pages * ($limit ?? self::DEFAULT_LIMIT));
		}));

		return $this->getEmbryosByIds($ids);
	}


	/**
	 * @return array[]
	 */
	public function getRepresentativeEmbryos(): array
	{
		$embryosByCluster = Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/embryos.neon'));

		$embryos = $this->getEmbryosByIds(array_values($embryosByCluster));

		return Nette\Utils\Arrays::map($embryosByCluster, function ($id) use ($embryos) {
			return $embryos[$id] ?? null;
		});
	}


	/**
	 * @return float[]
	 */
	public function splitCurve(string $curve): array
	{
		$trimmed = trim($curve);
		return $trimmed !== '' ? array_map('floatval', Nette\Utils\Strings::split($trimmed, '/\s*,\s*/')) : [];
	}


	/**
	 * @return array[][]
	 */
	public function getAnnotations($ids): array
	{
		$annotationsById = array_fill_keys($ids, []);

		$annotations = $this->catiDatabase->executeQuery('
			SELECT EmbryoId, Image, CellCount, Error
			FROM Annotations
			WHERE EmbryoId IN (:ids)
			ORDER BY EmbryoId, Image, CellCount
		', [
			'ids' => $ids,
		], [
			'ids' => Kdyby\Doctrine\Connection::PARAM_INT_ARRAY,
		]);

		foreach ($annotations as $annotation) {
			$annotationsById[$annotation['EmbryoId']][] = $annotation;
		}

		return $annotationsById;
	}


	/**
	 * @return array[]
	 */
	public function formatEvents(array $annotations, int $expansion, string $collapses, array $expansionCurve): array
	{
		$cellCounts = [];
		$errors = [];
		foreach ($annotations as $annotation) {
			$image = (int) $annotation['Image'];
			$cellCounts[$image] = max((int) $annotation['CellCount'], $cellCounts[$image] ?? 0);
			$errors[$cellCounts[$image]] = ((int) $annotation['Error'] !== 0) || ($errors[$cellCounts[$image]] ?? false);
		}
		ksort($cellCounts);

		$maxCount = 1;
		$events = [];
		foreach ($cellCounts as $image => $cellCount) {
			if ($cellCount > $maxCount) {
				$events[$image][] = ['type' => 'division', 'cell_count' => $cellCount, 'error' => $errors[$cellCount]];
				$maxCount = $cellCount;
			}
		}

		if ($expansion >= 0) {
			$events[$expansion][] = ['type' => 'expansion'];

			foreach (Nette\Utils\Strings::split(trim($collapses), '/\s*,\s*/') as $collapse) {
				if ($collapse === '') {
					continue;
				}

				[$end, $start] = Nette\Utils\Strings::split($collapse, '/\s*:\s*/') + [null, null];
				if ($start !== null && $end !== null) {
					if ((int) $start > $expansion) {
						$min = $this->findMinimum((int) $start, (int) $end, $expansionCurve);
						$type = $this->getCollapseType((int) $start, $expansionCurve);
						$events[$min][] = ['type' => 'collapse', 'collapseType' => $type, 'start' => (int) $start, 'end' => (int) $end];
					}
				} elseif ($end !== null) {
					if ((int) $end > $expansion) {
						$type = $this->getCollapseType((int) $end, $expansionCurve);
						$events[(int) $end][] = ['type' => 'collapse', 'collapseType' => $type];
					}
				}
			}
		}

		ksort($events);
		return $events;
	}


	private function queryEmbryos(?array $conditions = null, ?int $offset = null, ?int $limit = null): \Doctrine\DBAL\Driver\Statement
	{
		$query = '
			SELECT
				CATIResultsEmbryos.EmbryoId,
				Embryos.CurveClass,
				Embryos.ImageCount,
				Patients.OffsetMinutes,
				CATIResultsEmbryos.expansionCurve,
				CATIResultsEmbryos.activityCurve,
				CATIResultsEmbryos.exp,
				CATIResultsEmbryos.collapses
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			JOIN Patients ON (Embryos.PatientId = Patients.PatientId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
		';
		$params = ['version' => $this->catiVersion];
		$types = [];

		if (isset($conditions['ids']) && is_array($conditions['ids'])) {
			$query .= "\t" . 'AND CATIResultsEmbryos.EmbryoId IN (:ids)' . "\n\t\t";
			$params['ids'] = $conditions['ids'];
			$types['ids'] = Kdyby\Doctrine\Connection::PARAM_INT_ARRAY;
		}

		if (isset($conditions['cluster']) && $conditions['cluster'] instanceof Cluster) {
			$query .= "\t" . 'AND Embryos.CurveClass = :cluster' . "\n\t\t";
			$params['cluster'] = $conditions['cluster']->getCatiId();
		}

		$query .= "\t" . 'ORDER BY CATIResultsEmbryos.EmbryoId' . "\n\t\t";

		if (isset($limit)) {
			$query .= "\t" . 'LIMIT :limit' . "\n\t\t";
			$params['limit'] = $limit;
			$types['limit'] = \PDO::PARAM_INT;
		}

		if (isset($offset)) {
			$query .= "\t" . 'OFFSET :offset' . "\n\t\t";
			$params['offset'] = $offset;
			$types['offset'] = \PDO::PARAM_INT;
		}

		return $this->catiDatabase->executeQuery($query, $params, $types);
	}


	private function getCollapseType(int $start, array $expansionCurve): string
	{
		$limits = self::COLLAPSE_CLASS_LIMITS;
		$classes = array_keys($limits);

		$reference = $expansionCurve[$start] ?? 0;
		if ($reference <= 0) {
			return $classes[count($classes) - 1];
		}

		$min = $reference;
		for ($i = 1; $i <= self::COLLAPSE_SCAN_LIMIT; $i++) {
			$value = $expansionCurve[$start + $i] ?? null;
			if (isset($value) && $value < $min) {
				$min = $value;
			}
		}

		$ratio = $min / $reference * 100;
		for ($i = 0; $i < count($classes) - 1; $i++) {
			if ($ratio < $limits[$classes[$i]]) {
				return $classes[$i];
			}
		}
		return $classes[count($classes) - 1];
	}


	private function findMinimum(int $start, int $end, array $expansionCurve): int
	{
		$min = null;
		$minValue = null;
		for ($i = $start + 1; $i <= $end; $i++) {
			$value = $expansionCurve[$i] ?? null;
			if (isset($value) && ($min === null || $value < $minValue)) {
				$min = $i;
				$minValue = $value;
			}
		}
		return $min ?? $end;
	}


	public function formatEmbryo(array $embryo, array $annotations, Cluster $cluster = null, string $phase = null): array
	{
		return array_merge(
			[
				'id' => $embryo['EmbryoId'],
				'image_url' => $this->imageDir . '/' . $embryo['EmbryoId'] . '.jpg',
				'last_image_url' => $this->imageDir . '/' . $embryo['EmbryoId'] . '_last.jpg',
				'image_count' => $embryo['ImageCount'],
				'image_offset_minutes' => $embryo['OffsetMinutes'],
				'expansion_data' => $expansion = $this->splitCurve($embryo['expansionCurve']),
				'activity_data' => $this->splitCurve($embryo['activityCurve']),
				'events' => $this->formatEvents(
					$annotations[$embryo['EmbryoId']],
					(int) $embryo['exp'],
					$embryo['collapses'],
					$expansion
				) ?: new \stdClass,
			],
			$cluster !== null ? ['cluster' => $cluster->getClassification()] : [],
			(string) $phase !== '' ? ['phase_of_development' => $phase] : []
		);
	}
}
