<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Netvor\Embryo\ApiModule\Model\EmbryoService;


class EmbryoDetailPresenter extends BaseLoggedInPresenter
{

	/** @var EmbryoService */
	private $model;


	public function __construct(EmbryoService $model)
	{
		$this->model = $model;
	}


	public function get(string $id, Nette\Application\Request $request): Nette\Application\IResponse
	{
		$embryo = $this->model->getEmbryoById((int) $id);
		$cluster = $embryo !== null ? $this->model->getClusterByCatiId($embryo['CurveClass']) : null;
		$annotations = $embryo !== null ? $this->model->getAnnotations([$embryo['EmbryoId']]) : null;
		$phase = $cluster !== null ? Nette\Utils\Strings::split($cluster->getName(), '~ with |/~')[0] : null;
		$learning = $this->model->getLearningDescriptions();
		$classification = $cluster !== null ? $cluster->getClassification() : null;
		$phaseClass = substr($classification, 0, 1);
		$qualityClass = substr($classification, 0, 2);

		return $embryo !== null && $cluster !== null && $annotations !== null
			? $this->json([
				'embryo' => $this->model->formatEmbryo($embryo, $annotations, $cluster, $phase),
				'cluster' => [
					'cluster' => $cluster->getCatiId(),
					'short_name' => $cluster->getShortName(),
					'learning_name' => $learning[$phaseClass]['byQuality'][$qualityClass]['byEvents'][$classification]['name']
						?? $cluster->getName(),
					'name' => $cluster->getName(),
					'description' => $cluster->getDescription(),
					'implantation_chance' => $cluster->getImplantationChance(),
					'color' => $colors[$cluster->getClassification()] ?? null,
					'values' => [],
				],
			])
			: null;
	}
}
