<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Application;
use Nette\Http;


abstract class BasePresenter implements Application\IPresenter
{
	use Nette\SmartObject;

	/** @var Http\IRequest @inject */
	public $httpRequest;

	/** @var Http\IResponse @inject */
	public $httpResponse;


	public function run(Application\Request $request): Application\IResponse
	{
		try {
			$id = $request->getParameter('id');
			if ($id !== null && !is_scalar($id)) {
				$message = sprintf('ID must be scalar, %s given.', is_object($id) ? get_class($id) : gettype($id));
				throw $this->error($message, Http\IResponse::S400_BAD_REQUEST);
			}

			$all = $id === null;
			switch (strtoupper($request->getMethod())) {
				case 'GET':
					return $all ? $this->getAll($request) : $this->get((string) $id, $request);
				case 'POST':
					if (!$all) {
						throw $this->methodNotSupported($request);
					}
					return $this->post($this->getData(), $request);
				case 'PUT':
					if ($all) {
						throw $this->methodNotSupported($request);
					}
					return $this->put((string) $id, $this->getData(), $request);
				case 'DELETE':
					if ($all) {
						throw $this->methodNotSupported($request);
					}
					return $this->delete((string) $id, $request);
				default:
					throw $this->methodNotSupported($request);
			}
		} catch (ErrorException $e) {
			return $this->errorJson($e->getMessage(), $e->getCode());
		}
	}


	public function get(string $id, Application\Request $request): Application\IResponse
	{
		throw $this->methodNotSupported($request);
	}


	public function getAll(Application\Request $request): Application\IResponse
	{
		throw $this->methodNotSupported($request);
	}


	public function post(array $data, Application\Request $request): Application\IResponse
	{
		throw $this->methodNotSupported($request);
	}


	public function put(string $id, array $data, Application\Request $request): Application\IResponse
	{
		throw $this->methodNotSupported($request);
	}


	public function delete(string $id, Application\Request $request): Application\IResponse
	{
		throw $this->methodNotSupported($request);
	}


	/**
	 * @param mixed $data
	 */
	protected function json($data): Application\Responses\JsonResponse
	{
		return new Application\Responses\JsonResponse($data);
	}


	protected function errorJson(string $message, int $code = 0): Application\Responses\JsonResponse
	{
		$this->httpResponse->setCode($code ?: Http\IResponse::S400_BAD_REQUEST);
		return $this->json($code !== 0 ? [
			'error' => true,
			'code' => $code,
			'message' => $message,
		] : [
			'error' => true,
			'message' => $message,
		]);
	}


	protected function error(string $message, int $code = 0): ErrorException
	{
		return new ErrorException($message, $code);
	}


	protected function methodNotSupported(Application\Request $request): ErrorException
	{
		$method = strtoupper($request->getMethod());
		$id = $request->getParameter('id') !== null ? (string) $request->getParameter('id') : 'null';
		$message = sprintf('Method %s (id=%s) is not supported.', $method, $id);
		return $this->error($message, Http\IResponse::S405_METHOD_NOT_ALLOWED);
	}


	/**
	 * @throws ErrorException
	 */
	private function getData(): array
	{
		$header = (string) $this->httpRequest->getHeader('Content-Type');
		if (($pos = strpos($header, ';')) !== false) {
			$header = substr($header, 0, $pos);
		}
		switch ($header) {
			case 'application/x-www-form-urlencoded':
			case 'multipart/form-data':
			case '':
				return $this->httpRequest->getPost();
			case 'application/json':
			case 'text/json':
				try {
					$data = Nette\Utils\Json::decode($this->httpRequest->getRawBody(), Nette\Utils\Json::FORCE_ARRAY);
				} catch (Nette\Utils\JsonException $e) {
					throw $this->error('Malformed JSON data', Http\IResponse::S400_BAD_REQUEST);
				}
				if (!is_array($data)) {
					$message = sprintf('JSON data must be array, %s given.', gettype($data));
					throw $this->error($message, Http\IResponse::S400_BAD_REQUEST);
				}
				return $data;
			default:
				$message = sprintf('Content type "%s" is not supported.', $header);
				throw $this->error($message, Http\IResponse::S415_UNSUPPORTED_MEDIA_TYPE);
		}
	}
}


class ErrorException extends \Exception
{
}
