<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Netvor;
use Netvor\Embryo\ApiModule\Model\EmbryoService;


class EmbryoPresenter extends BaseLoggedInPresenter
{
	private const CACHE_EXPIRATION = '1 day';

	/** @var EmbryoService */
	private $model;

	/** @var Nette\Caching\Cache */
	private $cache;


	public function __construct(EmbryoService $model, Nette\Caching\Cache $cache)
	{
		$this->model = $model;
		$this->cache = $cache;
	}


	public function getAll(Nette\Application\Request $request): Nette\Application\IResponse
	{
		$limit = Netvor\Embryo\Utils\Validator::validateField($request->getParameters(), 'limit', 'numericint:1..', $errors, false);
		$embryoId = Netvor\Embryo\Utils\Validator::validateField($request->getParameters(), 'embryoId', 'numericint:1..', $errors, false);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		if ($embryoId === null) {
			$key = [__CLASS__, __METHOD__, $limit !== null ? (int) $limit : EmbryoService::DEFAULT_LIMIT];
			if (($cached = $this->cache->load($key)) !== null) {
				return $this->json($cached);
			}
		}

		$clusters = $clusterPhaseIndices = $embryosByPhase = [];
		$colors = $this->model->getClusterColors();
		$learning = $this->model->getLearningDescriptions();
		foreach ($this->model->getClusters() as $cluster) {
			$clusters[$cluster->getCatiId()] = $cluster;
			$phase = Nette\Utils\Strings::split($cluster->getName(), '~ with |/~')[0];
			$clusterPhaseIndices[$cluster->getId()] = count($embryosByPhase[$phase] ?? []);

			$classification = $cluster->getClassification();
			$phaseClass = substr($classification, 0, 1);
			$qualityClass = substr($classification, 0, 2);
			$embryosByPhase[$phase][] = [
				'cluster' => $classification,
				'short_name' => $cluster->getShortName(),
				'learning_name' => $learning[$phaseClass]['byQuality'][$qualityClass]['byEvents'][$classification]['name'] ??
								   $cluster->getName(),
				'name' => $cluster->getName(),
				'description' => $cluster->getDescription(),
				'implantation_chance' => $cluster->getImplantationChance(),
				'color' => $colors[$cluster->getClassification()] ?? null,
				'values' => [],
			];
		}

		$embryos = $this->model->getEmbryosByClusters(array_keys($clusters), $limit !== null ? (int) $limit : null, $embryoId !== null ? (int) $embryoId : null);
		$annotations = $this->model->getAnnotations(array_column($embryos, 'EmbryoId'));
		foreach ($embryos as $embryo) {
			$cluster = $clusters[$embryo['CurveClass']];
			$phase = Nette\Utils\Strings::split($cluster->getName(), '~ with |/~')[0];
			$idx = $clusterPhaseIndices[$cluster->getId()];
			$embryosByPhase[$phase][$idx]['values'][] = $this->model->formatEmbryo($embryo, $annotations, $cluster, $phase);
		}

		return $this->json($embryoId === null && isset($key) ? $this->cache->save($key, [
			'embryosByCategories' => $embryosByPhase,
		], [
			Nette\Caching\Cache::EXPIRE => self::CACHE_EXPIRATION,
		]) : [
			'embryosByCategories' => $embryosByPhase,
		]);
	}


	public function get(string $id, Nette\Application\Request $request): Nette\Application\IResponse
	{
		$parameters = $request->getParameters();
		$offset = Netvor\Embryo\Utils\Validator::validateField($parameters, 'offset', 'numericint:0..', $errors, false);
		$limit = Netvor\Embryo\Utils\Validator::validateField($parameters, 'limit', 'numericint:1..', $errors, false);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		$cluster = $this->model->getCluster($id);
		if ($cluster === null) {
			throw $this->error('Cluster not found.', 404);
		}

		$key = [__CLASS__, __METHOD__, $cluster->getId(), $offset !== null ? (int) $offset : 0, $limit !== null ? (int) $limit : EmbryoService::DEFAULT_LIMIT];
		if (($cached = $this->cache->load($key)) !== null) {
			return $this->json($cached);
		}

		$embryos = $this->model->getEmbryosByCluster($cluster, $offset !== null ? (int) $offset : null, $limit !== null ? (int) $limit : null);
		$annotations = $this->model->getAnnotations(array_column($embryos, 'EmbryoId'));
		$phase = Nette\Utils\Strings::split($cluster->getName(), '~ with |/~')[0];
		return $this->json($this->cache->save($key, [
			'category' => $cluster->getDevelopmentPhase(),
			'cluster' => $cluster->getClassification(),
			'values' => array_map(function (array $embryo) use ($cluster, $annotations, $phase) {
				return $this->model->formatEmbryo($embryo, $annotations, $cluster, $phase);
			}, $embryos),
		], [
			Nette\Caching\Cache::EXPIRE => self::CACHE_EXPIRATION,
		]));
	}
}
