<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Netvor\Embryo\Model\CacheCleanService;


class AppPresenter extends BaseLoggedInPresenter
{
	/** @var CacheCleanService @inject */
	public $cacheCleanService;


	public function handleCleanCache()
	{
		if (!$this->cacheCleanService->isAuthorized($this->getUser())) {
			$this->flashMessage('You are not authorized to do this.', 'danger');
			return;
		}

		$this->cacheCleanService->clean();
		$this->flashMessage('The results were successfully refreshed.', 'success');
		$this->redirect('this', ['path' => $this->getParameter('path')]);
	}


	public function renderDefault()
	{
		$this->template->cleanCacheButton = $this->cacheCleanService->isAuthorized($this->getUser());
	}
}
