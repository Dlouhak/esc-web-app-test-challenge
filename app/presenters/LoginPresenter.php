<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Nette;
use Nette\Application\UI\Form;


class LoginPresenter extends BasePresenter
{
	private $backlink;


	public function actionDefault($backlink = null)
	{
		$this->backlink = $backlink !== null ? (string) $backlink : null;
		$this->checkUser();
	}


	protected function createComponentLoginForm()
	{
		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again');

		$form->addEmail('email', 'E-mail')
			->setRequired('Please enter your e-mail address')
			->addRule(Form::EMAIL, 'Please enter a valid e-mail address');

		$form->addPassword('password', 'Password')
			->setRequired('Please enter your password');

		$form->addCheckbox('remember', 'Remember me');

		$form->addSubmit('submit', 'Log in');
		$form->onSuccess[] = [$this, 'loginFormSucceeded'];

		return $form;
	}


	public function loginFormSucceeded(Form $form, $values)
	{
		try {
			$this->getUser()->setExpiration(($values['remember']) ? '2 weeks' : '2 hours');
			$this->getUser()->login($values->email, $values->password);
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('The username or password you entered is incorrect');
			return;
		}
		$this->checkUser();
		$this->redirect('this');
	}


	private function checkUser()
	{
		if ($this->getUser()->isLoggedIn()) {
			if ($this->backlink) {
				$this->restoreRequest($this->backlink);
			}

			$this->redirect('App:');
		}
	}
}
