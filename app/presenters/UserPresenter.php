<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Nette\Application\UI\Form;
use Nette\Utils\Arrays;
use Netvor\Embryo\Model\ClinicService;
use Netvor\Embryo\Model\Entities\Clinic;
use Netvor\Embryo\Model\UserService;


class UserPresenter extends BaseLoggedInPresenter
{
	/**
	 * @var UserService
	 * @inject
	 */
	public $userModel;

	/**
	 * @var ClinicService
	 * @inject
	 */
	public $clinicModel;


	public function renderDefault()
	{
		$user = $this->getUserIdentity();

		$this['settingsForm']->setDefaults([
			'firstName' => $user->getFirstName(),
			'lastName' => $user->getLastName(),
			'clinic' => $user->getClinic() !== null ? $user->getClinic()->getId() : null,
		]);
	}


	public function createComponentSettingsForm()
	{
		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again.');

		$form->addText('firstName', 'First name')
			->setRequired('Please enter your first name');

		$form->addText('lastName', 'Last name')
			->setRequired('Please enter your last name');

		$form->addSelect('clinic', 'Clinic where you work', [
				null => 'I am an independent embryologist',
			] + Arrays::associate(Arrays::map($this->clinicModel->getAll(), function (Clinic $clinic) {
				return [
					'id' => $clinic->getId(),
					'title' => $clinic->getName(),
				];
			}), 'id=title'));

		$form->addCheckbox('changePassword', 'Change Password');

		$form->addPassword('passwordOld', 'Current password')
			->setOmitted(true)
			->addConditionOn($form['changePassword'], Form::FILLED)
			->setRequired('Please enter your current password')
			->addRule(function ($input) {
				return $this->getUserIdentity()->verifyPassword($input->value);
			}, 'Password is incorrect');

		$form->addPassword('password', 'New password')
			->addConditionOn($form['changePassword'], Form::FILLED)
			->setRequired('Please enter your new password');

		$form->addPassword('passwordConfirm', 'New password (again)')
			->setOmitted(true)
			->addConditionOn($form['changePassword'], Form::FILLED)
			->setRequired('Please confirm your new password')
			->addRule(Form::EQUAL, 'The passwords do not match', $form['password']);

		$form->addSubmit('submit', 'Update settings');
		$form->onSuccess[] = [$this, 'settingsFormSucceeded'];
		return $form;
	}


	public function settingsFormSucceeded(Form $form, $values)
	{
		$password = $values->changePassword ? $values->password : null;
		$clinic = $values->clinic !== '' ? $this->clinicModel->get($values->clinic) : null;
		$this->userModel->update($this->getUserIdentity(), $clinic, $values->firstName, $values->lastName, $password);
		$this->flashMessage('Your settings were successfully saved.', 'success');
		$this->redirect('this');
	}
}
