<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;


class LogoutPresenter extends BasePresenter
{
	public function actionDefault()
	{
		if ($this->getUser()->isLoggedIn()) {
			$this->getUser()->logout();
			$this->flashMessage('You have been logged out.');
		}
		$this->redirect('Login:');
	}
}
