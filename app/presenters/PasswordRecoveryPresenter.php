<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Nette\Application\UI\Form;
use Netvor\Embryo\Model\UserPasswordService;
use Netvor\Embryo\Model\UserService;

class PasswordRecoveryPresenter extends BasePresenter
{

	/**
	 * @var UserPasswordService
	 * @inject
	 */
	public $passwordModel;

	/**
	 * @var UserService
	 * @inject
	 */
	public $userModel;

	private $code;


	public function actionResetPassword($email, $code)
	{
		$this->code = $this->checkCode($email, $code);
	}


	protected function createComponentForgottenPasswordForm()
	{
		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again');

		$form->addEmail('email', 'E-mail')
			->setRequired('Please enter your e-mail address')
			->addRule(Form::EMAIL, 'Please enter a valid e-mail address');

		$form->addSubmit('submit', 'Send Recovery Link');
		$form->onSuccess[] = [$this, 'forgottenPasswordFormSucceeded'];

		return $form;
	}


	public function forgottenPasswordFormSucceeded(Form $form, $values)
	{
		$user = $this->userModel->getByEmail($values->email);
		if ($user === null) {
			$form->addError('There is no user with given e-mail');
			return;
		}

		$this->passwordModel->createCode($user);
		$this->redirect('emailSent');
	}


	protected function createComponentResetPasswordForm()
	{
		if ($this->code === null) {
			$this->error();
		}

		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again');

		$form->addPassword('password', 'New password')
			->setRequired('Please enter the new password');

		$form->addPassword('passwordConfirm', 'New password (again)')
			->setRequired('Please confirm the new password')
			->addRule(Form::EQUAL, 'The passwords do not match', $form['password']);

		$form->addSubmit('submit', 'Confirm');
		$form->onSuccess[] = [$this, 'resetPasswordFormSucceeded'];

		return $form;
	}


	public function resetPasswordFormSucceeded(Form $form, $values)
	{
		if ($this->code === null) {
			$this->error();
		}

		$this->passwordModel->resetPassword($this->code, $values->password);
		$this->flashMessage('Your password was successfuly changed');
		$this->redirect('Login:');
	}


	private function checkCode($email, $code)
	{
		$user = $this->userModel->getByEmail($email);
		if ($user === null) {
			$this->flashMessage('There is no user with given e-mail.', 'danger');
			$this->redirect('default');
		}

		$codeEntity = $this->passwordModel->getCode($user, $code);
		if ($codeEntity === null) {
			$this->flashMessage('The confirmation code is not valid.', 'danger');
			$this->redirect('default');
		}

		return $codeEntity;
	}
}
